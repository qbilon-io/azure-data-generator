package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.PostgreSQLServerConstants.RESOURCE_NAME_POSTGRESQL_SERVER;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.constants.azure.common.SQLCommonConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class PostgreSQLServer implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_POSTGRESQL_SERVER;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_POSTGRESQL_SERVER,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(ANY_IP_ACCESS_ALLOWED, AzureDataHelper.randomBool()),
            property(AUTOGROW_STORAGE, "autogrow"),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(DOMAIN_NAME, "database.windows.net"),
            property(EARLIEST_RESTORE_DATE, AzureDataHelper.randomDate()),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(LOCATION, "GER"),
            property(NAME, AzureDataHelper.randomName("Network-Security-Group")),
            property(PUBLIC_NETWORK_ACCESS_ALLOWED, AzureDataHelper.randomBool()),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(SKU_CAPACITY, AzureDataHelper.randomInt(100_000)),
            property(SKU_NAME, SKU_NAME),
            property(SSL_ENFORCEMENT_STATE, "enforced"),
            property(STORAGE_MB, AzureDataHelper.randomInt(1_000_000)),
            property(TYPE, "PostgreSQL"),
            property(VERSION, "1.13")
        );
    }
}
