package io.qbilon.azure.data.generator.data.input;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Edge extends Element {
  /** The datasource-specific unique ID of the source node of this edge */
  @JsonProperty("_from")
  public String sourceId;
  /** The datasource-specific unique ID of the target node of this edge */
  @JsonProperty("_to")
  public String targetId;

  public static Edge edge(
      String id,
      String type,
      String nameAttribute,
      String sourceId,
      String taregtId,
      Property... properties) {
    Edge edge = new Edge();
    edge.id = id;
    edge.type = type;
    edge.nameAttribute = nameAttribute;
    edge.sourceId = sourceId;
    edge.targetId = taregtId;
    edge.properties =
        Stream.of(properties)
            .filter(Objects::nonNull)
            .collect(Collectors.toMap(prop -> prop.name, prop -> prop));
    edge.creationTime = LocalDateTime.now().withSecond(0).withNano(0);
    return edge;
  }

  public static Edge edge(
      String id,
      String type,
      String nameAttribute,
      String sourceId,
      String taregtId,
      List<Property> properties) {
    Edge edge = new Edge();
    edge.id = id;
    edge.type = type;
    edge.nameAttribute = nameAttribute;
    edge.sourceId = sourceId;
    edge.targetId = taregtId;
    edge.properties =
        properties.stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toMap(prop -> prop.name, prop -> prop));
    edge.creationTime = LocalDateTime.now().withSecond(0).withNano(0);
    return edge;
  }
}
