package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.NAME;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_NAME_REGION;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;

public class Region implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_REGION;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_REGION,
            NAME,
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("Region"))
        );
    }
}
