package io.qbilon.azure.data.generator.constants.azure;

public class CosmosDBConstants {
  public static final String RESOURCE_NAME_COSMOS_DB_ACCOUNT = "Azure CosmosDB Account";
  public static final String RESOURCE_NAME_COSMOS_DB_DATABASE = "Azure CosmosDB Database";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String KEY = "Key";
  public static final String MULTIPLE_WRITE_LOCATIONS_ENABLED = "Multiple Write Locations Enabled";
  public static final String PUBLIC_NETWORK_ACCESS_ALLOWED = "Public Network Access Allowed";
  public static final String ANY_IP_ACCESS_ALLOWED = "Any IP Access Allowed";
  public static final String CREATION_MODE = "Creation Mode";
  public static final String LOCAL_AUTH_DISABLED = "Local Auth Disabled";
  public static final String CREATED_AT = "Created At";
  public static final String CREATED_BY = "Created By";
  public static final String VIRTUAL_NETWORK_FILTER_ENABLED = "Virtual Network Filter Enabled";
  public static final String KIND = "Kind";
  public static final String TYPE = "Type";
  public static final String NUMBER_COLUMNS = "Number Columns";
  public static final String TABLE_RID = "Table RID";
}
