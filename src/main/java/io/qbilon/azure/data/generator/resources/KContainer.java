package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.constants.kubernetes.K8sCommonConstants.*;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sContainerConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class KContainer implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_ID_CONTAINER;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_ID_CONTAINER,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(ARGS, "[arg2 | arg3 | arg5]"),
            property(CPU_USAGE, AzureDataHelper.randomInt(100)),
            property(IMAGE, IMAGE),
            property(IMAGE_PULL_POLICY, IMAGE_PULL_POLICY),
            property(MEMORY_USAGE, AzureDataHelper.randomInt(100)),
            property(NAME, AzureDataHelper.randomName("Kubernetes-Cluster"))
        );
    }
}
