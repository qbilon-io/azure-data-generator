package io.qbilon.azure.data.generator.constants.azure;

public class VirtualMachineScaleSetConstants {
  public static final String RESOURCE_NAME_VIRTUAL_MACHINE_SCALE_SET =
      "Azure Virtual Machine Scale Set";
  public static final String NAME = "Name";
  public static final String TYPE = "Type";
  public static final String OS_TYPE = "OS Type";
  public static final String COMPUTER_NAME_PREFIX = "Computer Name Prefix";
  public static final String SKU = "SKU";
  public static final String CAPACITY = "Capacity";
  public static final String OS_DISK_NAME = "OS Disk Name";
  public static final String RELATION_NAME_MANAGED_BY = "managed by";
  public static final String NUMBER_INSTANCES = "Number of instances";
}
