package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.constants.azure.CosmosDBConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class CosmosDBDatabase implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_COSMOS_DB_DATABASE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_COSMOS_DB_DATABASE,
            TABLE_RID,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NUMBER_COLUMNS, AzureDataHelper.randomInt(200)),
            property(TABLE_RID, AzureDataHelper.randomId())
        );
    }
}
