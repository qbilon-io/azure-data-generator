package io.qbilon.azure.data.generator.data.input;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Node extends Element {

  public static Node node(String id, String type, String nameAttribute, Property... properties) {
    Node node = new Node();
    node.id = id;
    node.type = type;
    node.nameAttribute = nameAttribute;
    node.properties =
        Stream.of(properties)
            .filter(Objects::nonNull)
            .collect(Collectors.toMap(prop -> prop.name, prop -> prop));
    node.creationTime = LocalDateTime.now().withSecond(0).withNano(0);
    return node;
  }

  public static Node node(String id, String type, String nameAttribute, List<Property> properties) {
    Node node = new Node();
    node.id = id;
    node.type = type;
    node.nameAttribute = nameAttribute;
    node.properties =
        properties.stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toMap(prop -> prop.name, prop -> prop));
    node.creationTime = LocalDateTime.now().withSecond(0).withNano(0);
    return node;
  }
}
