package io.qbilon.azure.data.generator.constants.kubernetes;

public class K8sCommonConstants {
  public static final String ID = "ID";
  public static final String NAME = "Name";
  public static final String RELATION_CONTAINS = "contains";
  public static final String RELATION_RUNS = "runs";
  public static final String RELATION_DEPLOYS = "deploys";
  public static final String RELATION_MOUNTS = "mounts";
  public static final String CPU_USAGE = "CPU Usage (vCPU)";
  public static final String MEMORY_USAGE = "Memory Usage (MB)";
}
