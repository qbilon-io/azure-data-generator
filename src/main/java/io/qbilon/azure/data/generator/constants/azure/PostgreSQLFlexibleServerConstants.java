package io.qbilon.azure.data.generator.constants.azure;

public class PostgreSQLFlexibleServerConstants {
  public static final String RESOURCE_NAME_POSTGRE_FLEXIBLE_SERVER =
      "Azure PostgreSQL Flexible Server";
}
