package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.StorageAccountConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class BlobContainer implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_BLOB_CONTAINER;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_BLOB_CONTAINER,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(DELETED, AzureDataHelper.randomBool()),
            property(HAS_PUBLIC_ACCESS, AzureDataHelper.randomBool()),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(LAST_MODIFIED_TIME, AzureDataHelper.randomDate()),
            property(NAME, AzureDataHelper.randomName("Blob-Container")),
            property(REMAINING_RETENTION_DAYS, AzureDataHelper.randomInt(100))
        );
    }
}
