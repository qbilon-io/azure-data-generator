package io.qbilon.azure.data.generator.constants.azure;

public class FrontDoorConstants {
  public static final String RESOURCE_NAME_FRONT_DOOR = "Front Door";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String SKU = "SKU";
  public static final String FRIENDLY_NAME = "Friendly Name";
  public static final String CNAME = "CName";
  public static final String ENABLED_STATE = "Enabled State";
  public static final String LOCATION = "Location";
  public static final String RESOURCE_STATE = "Resource State";
}
