package io.qbilon.azure.data.generator.constants.azure;

public class MySQLFlexibleServerConstants {
  public static final String RESOURCE_NAME_MY_SQL_FLEXIBLE_SERVER = "Azure MySQL Flexible Server";
  public static final String STORAGE_SKU = "Storage SKU";
}
