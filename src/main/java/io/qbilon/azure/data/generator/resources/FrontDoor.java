package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.FrontDoorConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class FrontDoor implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_NAME_FRONT_DOOR;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_FRONT_DOOR,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            azureTag(TAG_UPDATED_BY_SCRIPT),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(ENABLED_STATE, AzureDataHelper.randomChoose("Enabled", "Disabled")),
            property(FRIENDLY_NAME, FRIENDLY_NAME),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(LOCATION, "GER"),
            property(NAME, AzureDataHelper.randomName("Front-Door")),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(RESOURCE_STATE, RESOURCE_STATE)
        );
    }
}
