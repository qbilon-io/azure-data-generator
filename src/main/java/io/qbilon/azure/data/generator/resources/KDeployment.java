package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.NAME;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sDeploymentConstants.RESOURCE_ID_DEPLOYMENT;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class KDeployment implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_ID_DEPLOYMENT;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_ID_DEPLOYMENT,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(AZURE_ID, AzureDataHelper.randomId()),
            k8sLabel(LABEL_ADDON_MANAGER_KUBERNETES_IO_MODE),
            k8sLabel(LABEL_APP),
            k8sLabel(LABEL_APP_KUBERNETES_IO_COMPONENT),
            k8sLabel(LABEL_APP_KUBERNETES_IO_INSTANCE),
            k8sLabel(LABEL_APP_KUBERNETES_IO_MANAGED_BY),
            k8sLabel(LABEL_APP_KUBERNETES_IO_NAME),
            k8sLabel(LABEL_APP_KUBERNETES_IO_PART_OF),
            k8sLabel(LABEL_APP_KUBERNETES_IO_VERSION),
            k8sLabel(LABEL_APPMANAGER),
            k8sLabel(LABEL_APPPUBLIC),
            k8sLabel(LABEL_APPREPLICATOR),
            k8sLabel(LABEL_AZURE_WORKLOAD_IDENTITY_IO_SYSTEM),
            k8sLabel(LABEL_AZURE_WORKLOAD_IDENTITY_USE),
            k8sLabel(LABEL_CHART),
            k8sLabel(LABEL_COMPONENT),
            k8sLabel(LABEL_CONTROL_PLANE),
            k8sLabel(LABEL_ENVIRONMENT),
            k8sLabel(LABEL_GATEKEEPER_SH_OPERATION),
            k8sLabel(LABEL_GATEKEEPER_SH_SYSTEM),
            k8sLabel(LABEL_HELM_SH_CHART),
            k8sLabel(LABEL_HELM_TOOLKIT_FLUXCD_IO_NAME),
            k8sLabel(LABEL_HELM_TOOLKIT_FLUXCD_IO_NAMESPACE),
            k8sLabel(LABEL_HERITAGE),
            k8sLabel(LABEL_K8S_APP),
            k8sLabel(LABEL_KUBERNETES_AZURE_COM_MANAGED_BY),
            k8sLabel(LABEL_KUBERNETES_IO_CLUSTER_SERVICE),
            k8sLabel(LABEL_KUBERNETES_IO_NAME),
            k8sLabel(LABEL_KUSTOMIZE_TOOLKIT_FLUXCD_IO_NAME),
            k8sLabel(LABEL_NAME),
            k8sLabel(LABEL_RELEASE),
            k8sLabel(LABEL_SERVICE),
            k8sLabel(LABEL_SUFFIX),
            k8sLabel(LABEL_TIER),
            property(NAME, AzureDataHelper.randomName("Kubernetes-Deployment"))
        );
    }
}
