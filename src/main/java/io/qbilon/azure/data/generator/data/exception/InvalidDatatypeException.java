package io.qbilon.azure.data.generator.data.exception;

public class InvalidDatatypeException extends RuntimeException {
  public InvalidDatatypeException(String msg) {
    super(msg);
  }
}
