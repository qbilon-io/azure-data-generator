package io.qbilon.azure.data.generator.constants.kubernetes;

public class K8sDeploymentConstants {
  public static final String RESOURCE_ID_DEPLOYMENT = "K8s Deployment";
}
