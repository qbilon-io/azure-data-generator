package io.qbilon.azure.data.generator.constants.azure;

public class LoadBalancerConstants {
  public static final String RESOURCE_NAME_LOAD_BALANCER = "Azure Load Balancer";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String SKU = "SKU";
  public static final String ROUTES_TO = "routes to";
}
