package io.qbilon.azure.data.generator.constants.azure;

public class StorageAccountConstants {
  public static final String RESOURCE_NAME_STORAGE_ACCOUNT = "Azure Storage Account";
  public static final String RESOURCE_NAME_BLOB_CONTAINER = "Azure Blob Container";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String CAN_ACCESS_FROM_AZURE_SERVICES = "Can Access From Azure Services";
  public static final String CAN_READ_LOG_ENTRIES_FROM_ANY_NETWORK =
      "Can Read Log Entries From Any Network";
  public static final String CAN_READ_METRICS_FROM_ANY_NETWORK =
      "Can Read Metrics From Any Network";
  public static final String IS_ACCESS_ALLOWED_FROM_ALL_NETWORKS =
      "Is Access Allowed From All Networks";
  public static final String AZURE_FILES_AAD_INTEGRATION_ENABLED =
      "Azure Files AAD Integration Enabled";
  public static final String HNS_ENABLED = "HNS Enabled";
  public static final String BLOB_PUBLIC_ACCESS_ALLOWED = "Blob Public Access Allowed";
  public static final String SHARED_KEY_ACCESS_ALLOWED = "Shared Key Access Allowed";
  public static final String HTTPS_TRAFFIC_ONLY = "HTTPS Traffic Only";
  public static final String LARGE_FILE_SHARES_ENABLED = "Large File Shares Enabled";
  public static final String KIND = "Kind";
  public static final String MINIMUM_TLS_VERSION = "Minimum TLS Version";
  public static final String SKU_TYPE = "SKU Type";
  public static final String PROVISIONING_STATE = "Provisioning State";
  public static final String AVERAGE_EGRESS_MB_7_D = "Average Egress (mb/7d)";
  public static final String AVERAGE_INGRESS_MB_7_D = "Average Ingress (mb/7d)";
  public static final String NUMBER_TRANSACTIONS_7_D = "Number Transactions / 7d";
  public static final String USED_CAPACITY_MB = "Used Capacity (mb)";
  public static final String DELETED = "Deleted";
  public static final String HAS_PUBLIC_ACCESS = "Has Public Access";
  public static final String REMAINING_RETENTION_DAYS = "Remaining Retention Days";
  public static final String LAST_MODIFIED_TIME = "Last Modified Time";
}
