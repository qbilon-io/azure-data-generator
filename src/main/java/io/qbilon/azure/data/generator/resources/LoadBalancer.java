package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.LoadBalancerConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class LoadBalancer implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_LOAD_BALANCER;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_LOAD_BALANCER,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_AKS_MANAGED_CLUSTER_NAME),
            azureTag(TAG_AKS_MANAGED_CLUSTER_RG),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("Load-Balancer")),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(SKU, AzureDataHelper.randomChoose("Standard_LRS", "StandardSSD_LRS"))
        );
    }
}
