package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.AppServiceConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class AppService implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_NAME_APP_SERVICE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_APP_SERVICE,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(AVAILABILITY_STATE, "Normal"),
            property(AVERAGE_CPU_SECONDS_7_D, AzureDataHelper.randomInt(100_000)),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(CLIENT_AFFINITY_ENABLED, AzureDataHelper.randomBool()),
            property(CLIENT_CERT_ENABLED, AzureDataHelper.randomBool()),
            property(CONTAINER_SIZE, AzureDataHelper.randomInt(1_000)),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(DAILY_MEMORY_TIME_QUOTA, AzureDataHelper.randomInt(10_000)),
            property(DEFAULT_HOSTNAME, "azurewebsites.net"),
            property(HTTPS_ONLY, AzureDataHelper.randomBool()),
            property(HYPERV_ENABLED, AzureDataHelper.randomBool()),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(LAST_MODIFIED_TIME, AzureDataHelper.randomDate()),
            property(MAX_NUMBER_OF_WORKERS, AzureDataHelper.randomInt(1_000)),
            property(NAME, AzureDataHelper.randomName("App-Service")),
            property(OPERATING_SYSTEM, AzureDataHelper.randomChoose("LINUX", "WINDOWS")),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(STATE, "RUNNING")
        );
    }
}
