package io.qbilon.azure.data.generator.constants.azure;

public class TrafficManagerConstants {
  public static final String RESOURCE_NAME_TRAFFIC_MANAGER = "Azure Traffic Manager";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String ROUTES_TO = "routes to";
  public static final String NAME = "Name";
  public static final String MONITORING_PATH = "Monitoring Path";
  public static final String DNS_LABEL = "DNS Label";
  public static final String MONITORING_PORT = "Monitoring Port";
  public static final String MONITOR_STATUS = "Monitor Status";
  public static final String ENABLED = "Enabled";
  public static final String TIME_TO_LIVE = "Time To Live";
  public static final String TRAFFIC_ROUTING_METHOD = "Traffic Routing Method";
  public static final String TYPE = "Type";
}
