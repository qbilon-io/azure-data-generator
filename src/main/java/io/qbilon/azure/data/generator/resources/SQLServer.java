package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.constants.azure.SQLServerConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class SQLServer implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_SQL_SERVER;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_SQL_SERVER,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(ANY_IP_ACCESS_ALLOWED, AzureDataHelper.randomBool()),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_DISPLAY_NAME),
            azureTag(TAG_INFORMATION_CLASSIFICATION),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(DOMAIN_NAME, "database.windows.net"),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(NAME, AzureDataHelper.randomName("SQLServer")),
            property(PUBLIC_NETWORK_ACCESS_ALLOWED, AzureDataHelper.randomBool()),
            property(TYPE, "SQL Server"),
            property(VERSION, 120)
        );
    }
}
