package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.NetworkSecurityGroupConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class NetworkSecurityGroup implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_NETWORK_SECURITY_GROUP;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_NETWORK_SECURITY_GROUP,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(ALLOWS_ALL_THROUGH_TRAFFIC, AzureDataHelper.randomBool()),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_PROJECT),
            azureTag(TAG_SOLUTION),
            azureTag(TAG_TEAM),
            azureTag(TAG_USER),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("Network-Security-Group")),
            property(OPEN_PORT_FOR_CIFS, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_DNS, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_FTP, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_HTTP, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_HTTPS, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_ELASTICSEARCH, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_MONGO_DB, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_MS_SQL, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_MY_SQL, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_ORACLE, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_POSTGESQL, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_RDP, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_RPC, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_SMTP, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_SSH, AzureDataHelper.randomBool()),
            property(OPEN_PORT_FOR_TELNET, AzureDataHelper.randomBool()),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE)
        );
    }
}
