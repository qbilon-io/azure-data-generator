package io.qbilon.azure.data.generator.data.datatype;

public enum DataSuperTypeName {
  Collection,
  Primitive
}
