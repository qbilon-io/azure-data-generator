package io.qbilon.azure.data.generator.data.datatype;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

public class DatatypeDeserializer extends StdDeserializer<Datatype> {

  public DatatypeDeserializer() {
    this(null);
  }

  public DatatypeDeserializer(Class<?> t) {
    super(t);
  }

  @Override
  public Datatype deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JacksonException {
    JsonNode node = p.getCodec().readTree(p);

    String datatypeNameString = node.asText();

    return Datatype.fromDatatypeName(datatypeNameString);
  }
}
