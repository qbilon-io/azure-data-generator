package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.SQLServerConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class SQLDatabase implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_SQL_DATABASE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_SQL_DATABASE,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(COLLATION, "SQL_Latin1_General_CP1_CI_AS"),
            property(CREATED_AT, AzureDataHelper.randomDate()),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(DATABASE_EDITION, "General Purpose"),
            property(IS_DATA_WAREHOUSE, AzureDataHelper.randomBool()),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(MAX_SIZE_BYTES, AzureDataHelper.randomInt(100_000)),
            property(NAME, AzureDataHelper.randomName("SQLDatabase")),
            property(SERVICE_LEVEL_OBJECTIVE_NAME, "GP_S_Gen5_2"),
            property(STATUS, AzureDataHelper.randomChoose("Paused", "Online")),
            property(THREAT_DETECTION_POLICY, AzureDataHelper.randomChoose("Enabled", "Disabled")),
            property(TRANSPARENT_DATA_ENCRYPTION, AzureDataHelper.randomChoose("Enabled", "Disabled"))
        );
    }
}
