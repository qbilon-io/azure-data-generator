package io.qbilon.azure.data.generator.constants.azure;

public class NetworkInterfaceConstants {
  public static final String RESOURCE_NAME_NETWORK_INTERFACE = "Azure Network Interface";
  public static final String RELATION_NAME_CONTAINS = "contains";

  public static final String RELATION_NAME_HAS_ATTACHED = "has attached";
  public static final String PRIVATE_IP_ADDRESS = "Private IP Address";
}
