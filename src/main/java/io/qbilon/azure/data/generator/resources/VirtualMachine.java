package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.constants.azure.VirtualMachineConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class VirtualMachine implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_VIRTUAL_MACHINE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_VIRTUAL_MACHINE,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        List<Property> properties = new ArrayList<>();
        properties.addAll(Arrays.asList(
            property(AVERAGE_CPU_7_D, AzureDataHelper.randomDouble(100)),
            property(AVERAGE_DISK_READ_IOPS_7_D, AzureDataHelper.randomInt(300)),
            property(AVERAGE_DISK_WRITE_IOPS_7_D, AzureDataHelper.randomInt(300)),
            azureTag(TAG_AKS_MANAGED_COORDINATION),
            azureTag(TAG_AKS_MANAGED_CREATE_OPERATION_ID),
            azureTag(TAG_AKS_MANAGED_CREATION_SOURCE),
            azureTag(TAG_AKS_MANAGED_KUBELET_IDENTITY_CLIENT_ID),
            azureTag(TAG_AKS_MANAGED_ORCHESTRATOR),
            azureTag(TAG_AKS_MANAGED_POOL_NAME),
            azureTag(TAG_AKS_MANAGED_RESOURCE_NAME_SUFFIX),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_PROJECT),
            azureTag(TAG_SOLUTION),
            azureTag(TAG_TEAM),
            azureTag(TAG_USER),
            property(CAUSE_OCCURRED_TIME, AzureDataHelper.randomDate()),
            property(COMPUTER_NAME, COMPUTER_NAME),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(CREATION_DATE, AzureDataHelper.randomDate()),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(DISK_DELETE_OPTION, "Detach"),
            property(HEALTH_STATUS_TYPE, "Customer Initiated"),
            property(HEALTH_SUMMARY, "There aren't any known Azure platform problems affecting this virtual machine."),
            property(HEALTH_TITLE, "Available"),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(LICENSE_TYPE, "GPL"),
            property(NAME, AzureDataHelper.randomName("VM")),
            property(NUMBER_DATA_DISKS, AzureDataHelper.randomInt(10)),
            property(NUMBER_SSH_KEYS, AzureDataHelper.randomInt(10)),
            property(OS_DISK_SIZE, AzureDataHelper.randomInt(10)),
            property(OS_VERSION, AzureDataHelper.randomDouble(3)),
            property(POWER_STATE, "running"),
            property(PUBLIC_IP_ADDRESS, AzureDataHelper.randomIP()),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(SIZE, "Standard_B8ms")
        ));

        properties.addAll(AzureDataHelper.randomChoose(
            Arrays.asList(
                property(LINUX_ASSESSMENT_MODE, "ImageDefault"),
                property(LINUX_PATCH_MODE, "Image Default"),
                property(OS_NAME, "Ubuntu"),
                property(OS_TYPE, "LINUX")
            ),
            Arrays.asList(
                property(WINDOWS_ASSESSMENT_MODE, "ImageDefault"),
                property(WINDOWS_PATCH_MODE, "Image Default"),
                property(OS_NAME, "Windows Server 2016"),
                property(OS_TYPE, "WINDOWS")
            )
        ));

        return properties;
    }
}
