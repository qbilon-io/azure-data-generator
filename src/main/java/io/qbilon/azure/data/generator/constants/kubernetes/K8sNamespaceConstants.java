package io.qbilon.azure.data.generator.constants.kubernetes;

public class K8sNamespaceConstants {
  public static final String RESOURCE_ID_NAMESPACE = "K8s Namespace";
}
