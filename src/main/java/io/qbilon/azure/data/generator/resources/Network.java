package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.NetworkConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class Network implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_NETWORK;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_NETWORK,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_PROJECT),
            azureTag(TAG_TEAM),
            azureTag(TAG_USER),
            property(DDOS_PROTECTION_ENABLED, AzureDataHelper.randomBool()),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("Network")),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE)
        );
    }
}
