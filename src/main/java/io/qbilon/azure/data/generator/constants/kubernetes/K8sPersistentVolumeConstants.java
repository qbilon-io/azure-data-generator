package io.qbilon.azure.data.generator.constants.kubernetes;

public class K8sPersistentVolumeConstants {
  public static final String RESOURCE_ID_PERSISTENT_VOLUME = "K8s Persistent Volumne";
}
