package io.qbilon.azure.data.generator.data.input;

import java.util.ArrayList;
import java.util.List;

public class Graph {
  public List<Node> nodes = new ArrayList<>();
  public List<Edge> edges = new ArrayList<>();

  public static Graph graph(List<Node> nodes, List<Edge> edges) {
    Graph g = new Graph();
    g.nodes = nodes;
    g.edges = edges;
    return g;
  }

  public static List<Node> nodes(Node... nodes) {
    return List.of(nodes);
  }

  public static List<Edge> edges(Edge... edges) {
    return List.of(edges);
  }
}
