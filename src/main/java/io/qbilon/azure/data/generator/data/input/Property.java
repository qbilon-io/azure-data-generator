package io.qbilon.azure.data.generator.data.input;

import io.qbilon.azure.data.generator.data.datatype.Datatype;

public class Property {
  /** Name of this property, e.g., 'Hostname' */
  public String name;
  /** The actual value of this property, e.g., qbilon.ad.io */
  public Object value;
  /** The datatype of the value of this property, e.g., String */
  public Datatype datatype;

  public static Property property(String name, Object value, Datatype datatype) {
    if (value == null) return null;
    Property prop = new Property();
    prop.name = name;
    prop.value = value;
    prop.datatype = datatype;
    return prop;
  }

  public static Property property(String name, Object value) {
    if (value == null) return null;
    Property prop = new Property();
    prop.name = name;
    prop.value = value;
    prop.datatype = new Datatype(value);
    return prop;
  }
}
