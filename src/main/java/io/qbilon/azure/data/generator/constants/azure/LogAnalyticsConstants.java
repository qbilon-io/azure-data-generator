package io.qbilon.azure.data.generator.constants.azure;

public class LogAnalyticsConstants {
  public static final String RESOURCE_NAME_LOG_ANALYTICS_WORKSPACE = "Log Analytics Workspace";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String SKU = "SKU";
  public static final String TYPE = "Type";
  public static final String ETAG = "ETag";
  public static final String LOCATION = "Location";
  public static final String PROVISIONING_STATE = "Provisioning State";
  public static final String RETENTION_IN_DAYS = "Retention In Days";
}
