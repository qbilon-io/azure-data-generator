package io.qbilon.azure.data.generator;

import io.qbilon.azure.data.generator.data.input.Graph;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.resources.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.qbilon.azure.data.generator.constants.kubernetes.K8sContainerConstants.*;
import static java.util.Map.entry;

public class AzureDataGenerator {

    private static final Map<AzureNodeGenerator, Integer> defaultNodeGenerators = Map.ofEntries(
        entry(new APIManagementService(), 3),
        entry(new AppService(), 1),
        entry(new AppServicePlan(), 7),
        entry(new BlobContainer(), 1_000),
        entry(new CosmosDBAccount(), 5),
        entry(new CosmosDBDatabase(), 4),
        entry(new Disk(), 20),
        entry(new FrontDoor(), 4),
        entry(new KCluster(), 1),
        entry(new KContainer(), 347),
        entry(new KDeployment(), 157),
        entry(new KImage(), 100),
        entry(new KNameSpace(), 10),
        entry(new KNodePool(), 1),
        entry(new KPod(), 260),
        entry(new KVolume(), 1_220),
        entry(new LoadBalancer(), 1),
        entry(new LogAnalysisWorkspace(), 10),
        entry(new Network(), 6),
        entry(new NetworkInterface(), 8),
        entry(new NetworkSecurityGroup(), 8),
        entry(new PostgreSQLFlexibleServer(), 1),
        entry(new PostgreSQLServer(), 1),
        entry(new PrivateEndpoint(), 2),
        entry(new PublicIPAddress(), 10),
        entry(new Region(), 6),
        entry(new ResourceGroup(), 200),
        entry(new SQLDatabase(), 30),
        entry(new SQLServer(), 4),
        entry(new StorageAccount(), 40),
        entry(new Subscription(), 5),
        entry(new TrafficManager(), 5),
        entry(new VirtualGateway(), 3),
        entry(new VirtualMachine(), 11),
        entry(new VirtualMachineScaleSet(), 1)
    );

    private final Map<AzureNodeGenerator, Integer> nodeGenerators;
    private final Map<String, Map<String, Node>> generatedNodesWithID = new HashMap<>();
    private final Map<String, List<Node>> generatedNodes = new HashMap<>();

    public AzureDataGenerator() {
        this.nodeGenerators = new HashMap<>(defaultNodeGenerators);
    }

    public AzureDataGenerator graphSize(int graphSize) {
        this.nodeGenerators.replaceAll((key, entry) -> entry * graphSize);
        return this;
    }

    public Graph generate() {
        Graph graph = new Graph();
        this.nodeGenerators.forEach((key, value) -> {
            Map<String, Node> nodes = new HashMap<>();
            for (int i = 0; i < value; i++) {
                Node node = key.generate();
                nodes.put(node.id, node);
            }

            this.generatedNodesWithID.put(key.type(), nodes);
            this.generatedNodes.put(key.type(), new ArrayList<>(nodes.values()));
        });

        // generate edges
        AzureDataHelper.Edges edges = new AzureDataHelper.Edges();
        containsEdges(edges);
        addTransitiveRGContainsEdges(edges);
        customEdges(edges);
        addRandomEdges(this.generatedNodes.get(RESOURCE_ID_CONTAINER), 5.0, "communicates with", edges);

        graph.nodes = this.generatedNodes.values().stream().flatMap(List::stream).collect(Collectors.toList());
        graph.edges = edges.edges();
        return graph;
    }

    public enum GraphSize {
        SMALL(1), MEDIUM(5), LARGE(10), EXTRA_LARGE(100);

        private final int factor;

        GraphSize(int factor) {
            this.factor = factor;
        }

        public int factor() {
            return factor;
        }
    }

    private void addRandomEdges(List<Node> nodes, double avgDegree, String edgeName, AzureDataHelper.Edges edges) {
        Random rand = new Random();
        Map<Node, Set<Node>> nodeNeighbors = nodes.stream().collect(Collectors.toMap(n -> n, n -> new HashSet<>()));
        Map<Node, Double> currentNodeDegree = nodes.stream().collect(Collectors.toMap(n -> n, n -> 0.0));
        Map<Node, Double> maxNodeDegree = nodes.stream().collect(Collectors.toMap(n -> n, n -> {
            boolean higherDegree = rand.nextBoolean();
            double relDiffDegree = rand.nextDouble() * 0.5;
            double absDiffDegree = avgDegree * relDiffDegree;
            double targetDegree = (higherDegree) ? avgDegree + absDiffDegree : avgDegree - absDiffDegree;
            return targetDegree;
        }));

        for (Node node : nodes) {
            double remainingEdges = maxNodeDegree.get(node) - currentNodeDegree.get(node);

            while (remainingEdges > 0.5) {
                Node newNeighbor = null;
                Set<Node> neighbors = nodeNeighbors.get(node);
                int maxNrRetries = 5;
                int currentRetries = 0;
                while(newNeighbor == null && currentRetries < maxNrRetries){
                    Node candidate = nodes.get(rand.nextInt(nodes.size()));
                    double remainingEdgesTargetNode = maxNodeDegree.get(candidate) - currentNodeDegree.get(candidate);
                    if(candidate != node && !neighbors.contains(candidate) && remainingEdgesTargetNode >= 0.5) {
                        newNeighbor = candidate; 
                    }
                    currentRetries++;
                }

                if(newNeighbor != null) {
                    edges.add(AzureDataHelper.defaultEdge(node.id, newNeighbor.id, edgeName));
                    currentNodeDegree.put(node, currentNodeDegree.get(node) + 1.0);
                    currentNodeDegree.put(newNeighbor, currentNodeDegree.get(newNeighbor) + 1.0);
                    neighbors.add(newNeighbor);
                }

                remainingEdges--;
            }
        }

    }

    /**
     * Edges
     */

    private void containsEdges(AzureDataHelper.Edges edges) {
        // Subscription
        List<Node> subscriptions = this.generatedNodes.get(AzureDataHelper.SUBSCRIPTION);
        this.generatedNodes.get(AzureDataHelper.RESOURCE_GROUP).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, subscriptions)));
        this.generatedNodes.get(AzureDataHelper.NETWORK).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, subscriptions)));
        this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, subscriptions)));

        // REGION
        List<Node> regions = this.generatedNodes.get(AzureDataHelper.REGION);
        this.generatedNodes.get(AzureDataHelper.API_MANAGEMENT_SERVICE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, regions)));
        this.generatedNodes.get(AzureDataHelper.RESOURCE_GROUP).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, regions)));

        // Resource Group
        List<Node> resourceGroups = this.generatedNodes.get(AzureDataHelper.RESOURCE_GROUP);
        this.generatedNodes.get(AzureDataHelper.NETWORK).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.COSMOS_DB_ACCOUNT).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.LOAD_BALANCER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.POSTGRE_SQL_SERVER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.POSTGRE_SQL_FLEXIBLE_SERVER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.LOG_ANALYSIS_WORKSPACE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.FRONT_DOOR).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.TRAFFIC_MANAGER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.SQL_SERVER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.APP_SERVICE_PLAN).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.K_CLUSTER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE_SCALE_SET).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.STORAGE_ACCOUNT).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.NETWORK_INTERFACE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.DISK).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));
        this.generatedNodes.get(AzureDataHelper.NETWORK_SECURITY_GROUP).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, resourceGroups)));

        // Network
        this.generatedNodes.get(AzureDataHelper.VIRTUAL_GATEWAY).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.NETWORK))));

        // CosmosDB
        this.generatedNodes.get(AzureDataHelper.COSMOS_DB_DATABASE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.COSMOS_DB_ACCOUNT))));

        // SQL Server
        this.generatedNodes.get(AzureDataHelper.SQL_DATABASE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.SQL_SERVER))));

        // App Service Plan
        this.generatedNodes.get(AzureDataHelper.APP_SERVICE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.APP_SERVICE_PLAN))));

        // K8s Cluster
        this.generatedNodes.get(AzureDataHelper.K_NAME_SPACE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.K_CLUSTER))));
        this.generatedNodes.get(AzureDataHelper.K_NODE_POOL).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.K_CLUSTER))));

        // Virtual Machine Scale Set
        this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE_SCALE_SET))));

        // Storage Account
        this.generatedNodes.get(AzureDataHelper.BLOB_CONTAINER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.STORAGE_ACCOUNT))));

        // Network Interface
        this.generatedNodes.get(AzureDataHelper.PRIVATE_ENDPOINT).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.NETWORK_INTERFACE))));

        // Virtual Machine
        this.generatedNodes.get(AzureDataHelper.PUBLIC_IP_ADDRESS).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE))));

        // K8s Namespace
        this.generatedNodes.get(AzureDataHelper.K_DEPLOYMENT).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.K_NAME_SPACE))));
        this.generatedNodes.get(AzureDataHelper.K_POD).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.K_NAME_SPACE))));

        // K8s Pod
        this.generatedNodes.get(AzureDataHelper.K_CONTAINER).forEach(el -> edges.add(AzureDataHelper.defaultContainsEdge(el, this.generatedNodes.get(AzureDataHelper.K_POD))));

        // K8s Container
        this.generatedNodes.get(AzureDataHelper.K_IMAGE).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.K_CONTAINER), "refers to")));
        this.generatedNodes.get(AzureDataHelper.K_VOLUME).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.K_CONTAINER), "mounts")));
    }

    public void addTransitiveRGContainsEdges(AzureDataHelper.Edges edges) {
        Set<String> resourceGroupIds = this.generatedNodesWithID.get(AzureDataHelper.RESOURCE_GROUP).keySet();
        Set<String> nodesBetweenIds = Stream.of(
            this.generatedNodesWithID.get(AzureDataHelper.NETWORK).keySet(),
            this.generatedNodesWithID.get(AzureDataHelper.SQL_SERVER).keySet(),
            this.generatedNodesWithID.get(AzureDataHelper.APP_SERVICE_PLAN).keySet(),
            this.generatedNodesWithID.get(AzureDataHelper.VIRTUAL_MACHINE_SCALE_SET).keySet(),
            this.generatedNodesWithID.get(AzureDataHelper.NETWORK_INTERFACE).keySet()
        ).flatMap(Collection::stream).collect(Collectors.toSet());
        AzureDataHelper.createTransitiveEdges(edges, resourceGroupIds, nodesBetweenIds);

        Set<String> vmIds = this.generatedNodesWithID.get(AzureDataHelper.VIRTUAL_MACHINE).keySet();
        AzureDataHelper.createTransitiveEdges(edges, resourceGroupIds, vmIds);
    }

    private void customEdges(AzureDataHelper.Edges edges) {
        this.generatedNodes.get(AzureDataHelper.NETWORK_SECURITY_GROUP).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.NETWORK_INTERFACE), "secured by")));
        this.generatedNodes.get(AzureDataHelper.K_POD).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE), "runs")));
        this.generatedNodes.get(AzureDataHelper.DISK).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE), "has attached")));
        this.generatedNodes.get(AzureDataHelper.K_NODE_POOL).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.VIRTUAL_MACHINE_SCALE_SET), "managed by")));
        this.generatedNodes.get(AzureDataHelper.K_POD).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.K_DEPLOYMENT), "deploys")));
        this.generatedNodes.get(AzureDataHelper.APP_SERVICE_PLAN).forEach(el -> edges.add(AzureDataHelper.defaultEdge(el, this.generatedNodes.get(AzureDataHelper.APP_SERVICE), "manages")));
    }
}
