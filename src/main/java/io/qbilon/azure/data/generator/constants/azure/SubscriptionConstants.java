package io.qbilon.azure.data.generator.constants.azure;

public class SubscriptionConstants {
  public static final String RESOURCE_NAME_SUBSCRIPTION = "Azure Subscription";
  public static final String NAME = "Name";
  public static final String STATE = "State";
}
