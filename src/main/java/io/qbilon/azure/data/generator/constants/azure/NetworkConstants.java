package io.qbilon.azure.data.generator.constants.azure;

public class NetworkConstants {
  public static final String RESOURCE_NAME_NETWORK = "Azure Network";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String DDOS_PROTECTION_ENABLED = "DDOS Protection Enabled";
}
