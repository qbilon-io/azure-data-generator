package io.qbilon.azure.data.generator.constants.azure;

public class KubernetesClusterConstants {
  public static final String RESOURCE_NAME_KUBERNETES_CLUSTER = "Kubernetes Cluster";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String DNS_PREFIX = "DNS Prefix";
  public static final String POWER_STATE = "Power State";
  public static final String VERSION = "Version";
  public static final String RESOURCE_NAME_NODE_POOL = "Kubernetes Node Pool";
  public static final String NUMBER_NODES = "Number Nodes";
  public static final String MAX_NODE_SIZE = "Max. Node Size";
  public static final String AUTOSCALING_ENABLED = "Autoscaling Enabled";
  public static final String KUBELET_DISK_TYPE = "Kubelet Disk Type";
  public static final String AGENT_POOL_MODE = "Agent Pool Mode";
  public static final String OS_DISK_SIZE_GB = "OS Disk Size (GB)";
  public static final String OS_TYPE = "OS Type";
  public static final String TYPE = "Type";
  public static final String VM_SIZE = "VM Size";
}
