package io.qbilon.azure.data.generator.data.datatype;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.qbilon.azure.data.generator.data.exception.InvalidDatatypeException;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

@JsonDeserialize(using = DatatypeDeserializer.class)
@JsonSerialize(using = DatatypeSerializer.class)
public class Datatype {
  private DataTypeName name;
  private List<Datatype> generics = new ArrayList<>();

  public Datatype(Object obj) {
    init(this, obj, obj.getClass());
  }

  private Datatype(DataTypeName name) {
    this.name = name;
  }

  private Datatype(DataTypeName name, Datatype... generics) {
    this.name = name;
    this.generics = List.of(generics);
  }

  static Datatype primitiveDatatype(DataTypeName name) {
    return new Datatype(name);
  }

  static Datatype listDatatype(DataTypeName name, Datatype generic) {
    return new Datatype(name, generic);
  }

  static Datatype mapDatatype(DataTypeName name, Datatype genericKey, Datatype genericValue) {
    return new Datatype(name, genericKey, genericValue);
  }

  public DataTypeName name() {
    return name;
  }

  public boolean isPrimitive() {
    return name.isPrimitive();
  }

  public boolean isList() {
    return name == DataTypeName.List;
  }

  public boolean isMap() {
    return name == DataTypeName.Map;
  }

  public List<Datatype> generics() {
    return generics;
  }

  @SuppressWarnings("rawtypes")
  private void init(Datatype dt, Object obj, Class<?> clazz) {
    if (clazz.isPrimitive()) {
      if (boolean.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Boolean;
        return;
      }
      if (int.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
      if (long.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
      if (float.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
      if (double.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
    } else {
      if (String.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Text;
        return;
      }
      if (Boolean.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Boolean;
        return;
      }
      if (Integer.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
      if (Long.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
      if (Float.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
      if (Double.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Number;
        return;
      }
      if (LocalDateTime.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.DateTime;
        return;
      }
      if (Map.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.Map;
        Map map = (Map) obj;
        if (map.size() == 0) {
          handleEmptyMap();
        } else {
          Object key = map.keySet().iterator().next();
          Object value = map.values().iterator().next();
          generics.add(new Datatype(key));
          generics.add(new Datatype(value));
        }
        return;
      }
      if (List.class.isAssignableFrom(clazz)) {
        dt.name = DataTypeName.List;
        List list = (List) obj;
        if (list.isEmpty()) {
          handleEmptyList();
        } else {
          Object item = list.get(0);
          generics.add(new Datatype(item));
        }
        return;
      }
    }
    throw new InvalidDatatypeException(
        "Class "
            + clazz.getSimpleName()
            + " cannot be used as data type, as it is no supported type");
  }

  protected void handleEmptyList() {
    throw new InvalidDatatypeException(
        "Empty list can not be used as type parameter as we can not determine its runtime value types");
  }

  protected void handleEmptyMap() {
    throw new InvalidDatatypeException(
        "Empty map can not be used as type parameter as we can not determine its runtime key/value types");
  }

  /**
   * Checks if this DataType is assignable to the given datatype dt.
   *
   * @param dt the datatype to be assigend to
   * @return true if this datatype is assignable to dt
   */
  public boolean isAssignableTo(Datatype dt) {
    // short circuit if types don't match
    if (!name().equals(dt.name())) return false;
    // short circuit if we reach this stage and names do match
    if (name.isPrimitive()) return true;

    // else we are in the case that we are a list or a map
    // in this case we have to regard some special cases,
    // where a runtime map or list might not have generic
    // parameters but are logically assignable to this type
    // that is more specific

    // the first list has no generics so the second one can be assigned
    if (generics.isEmpty()) return true;

    // if both have generics information we can short circuit if
    // they differ in size
    if (generics.size() != dt.generics.size()) return false;

    // if they don't differ we can recursively traverse them and
    // test if they are assignable to each other
    for (int i = 0; i < this.generics.size(); i++) {
      Datatype thisGen = this.generics.get(i);
      Datatype otherGen = dt.generics.get(i);
      // short circuit if one generic does not match
      if (!thisGen.isAssignableTo(otherGen)) return false;
    }
    // if all checks passed the type is assignable to the other one
    return true;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(name.toString());
    if (!generics.isEmpty()) {
      sb.append("<");
      sb.append(generics.stream().map(Datatype::toString).collect(Collectors.joining(", ")));
      sb.append(">");
    }
    return sb.toString();
  }

  public boolean isEqualTo(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Datatype datatype = (Datatype) o;
    return this.toString().equals(datatype.toString());
  }

  /**
   * Returns a new datatype based in its string representation
   *
   * @param datatypename The representation of the datatype as a string
   * @return a new corresponding datatype
   */
  public static Datatype fromDatatypeName(String datatypename) {
    datatypename = datatypename.toLowerCase(Locale.ROOT);
    return new Datatype(fromStringDatatypeName(datatypename));
  }

  private static String removePrefixFromString(String datatype) {
    String prefix = datatype.replaceFirst("list<", "");
    return prefix.substring(0, prefix.length() - 1);
  }

  public static Object fromStringDatatypeName(String datatypename) {
    if (datatypename.startsWith(DataTypeName.List.toString().toLowerCase()))
      return List.of(fromStringDatatypeName(removePrefixFromString(datatypename)));
    if (datatypename.equals(DataTypeName.Boolean.toString().toLowerCase())) return false;
    if (datatypename.equals(DataTypeName.Number.toString().toLowerCase())) return 1d;
    if (datatypename.equals(DataTypeName.Text.toString().toLowerCase())) return "";
    if (datatypename.equals(DataTypeName.DateTime.toString().toLowerCase()))
      return LocalDateTime.now();
    throw new InvalidDatatypeException(
        "Datatypename " + datatypename + " cannot be converted to a valid datatype");
  }

  public static Object convertStringValueToObject(String value) {
    if (value.isBlank()) return null;
    // Boolean
    try {
      return convertStringToBoolean(value);
    } catch (UnknownFormatConversionException ignore) {
    }
    // Double
    try {
      return convertStringToDouble(value);
    } catch (NumberFormatException ignore) {
    }

    // LocalDateTime
    try {
      return convertStringToDateTime(value);
    } catch (DateTimeParseException ignore) {
    }

    // List
    if (value.startsWith("[") && value.endsWith("]")) {
      // special case - the list does not contain any values, only the brackets
      if (value.length() == 2) return null;
      // return values as list
      var list = Arrays.asList(value.substring(1, value.length() - 1).split("[|]"));
      return list.stream()
          .map(v -> convertStringValueToObject(v.trim()))
          .collect(Collectors.toList());
    }
    // finally, it must be a string
    return value;
  }

  private static Boolean convertStringToBoolean(String value) throws InvalidDatatypeException {
    if (value.equalsIgnoreCase("true")) return Boolean.TRUE;
    if (value.equalsIgnoreCase("false")) return Boolean.FALSE;
    throw new UnknownFormatConversionException("Can't convert: " + value + " to a boolean");
  }

  private static Double convertStringToDouble(String value) throws NumberFormatException {
    return Double.parseDouble(value);
  }

  private static LocalDateTime convertStringToDateTime(String value) throws DateTimeParseException {
    return LocalDateTime.parse(value);
  }

  private static List<Object> convertStringToList(String value, DataTypeName type) {
    if (value.startsWith("[") && value.endsWith("]")) {
      // special case - the list does not contain any values, only the brackets
      if (value.length() == 2) return null;
      // return values as list
      var list = Arrays.asList(value.substring(1, value.length() - 1).split("[|]"));
      List<Object> convertedEntry = new ArrayList<>();
      for (String entry : list) {
        Object convertedValue = convertStringValueToPrimitiveType(entry, type);
        if (convertedValue == null) {
          return null;
        }
        convertedEntry.add(convertedValue);
      }
      return convertedEntry;
    }
    return null;
  }

  public static Object convertStringValueToSpecificDatatype(String value, String type)
      throws InvalidDatatypeException {
    DataTypeName datatype;
    if (type.matches("List<.+>")) {
      datatype = DataTypeName.List;
    } else {
      datatype = DataTypeName.valueOf(type);
    }
    if (datatype.isPrimitive()) {
      return convertStringValueToPrimitiveType(value, datatype);
    } else {
      if (datatype == DataTypeName.List) {
        DataTypeName listDatatype =
            DataTypeName.valueOf(type.substring(type.indexOf("<") + 1, type.indexOf(">")));
        if (!listDatatype.isPrimitive()) {
          throw new InvalidDatatypeException(datatype.name() + " is not a primitive type");
        }
        return convertStringToList(value, listDatatype);
      }
      // fallback for maps
      return convertStringValueToObject(value);
    }
  }

  private static Object convertStringValueToPrimitiveType(String value, DataTypeName datatype)
      throws InvalidDatatypeException {
    try {
      switch (datatype) {
        case Number:
          return convertStringToDouble(value);
        case Text:
          return value;
        case DateTime:
          return convertStringToDateTime(value);
        case Boolean:
          return convertStringToBoolean(value);
        case Map:
        case List:
          throw new InvalidDatatypeException(datatype.name() + " is not a primitive type");
        default:
          return convertStringValueToObject(value);
      }
    } catch (DateTimeParseException | NumberFormatException | UnknownFormatConversionException e) {
      return null;
    }
  }

  public static Datatype text() {
    return primitiveDatatype(DataTypeName.Text);
  }

  public static Datatype bool() {
    return primitiveDatatype(DataTypeName.Boolean);
  }

  public static Datatype number() {
    return primitiveDatatype(DataTypeName.Number);
  }

  public static Datatype date() {
    return primitiveDatatype(DataTypeName.DateTime);
  }

  public static Datatype list(Datatype generic) {
    return listDatatype(DataTypeName.List, generic);
  }

  public static Datatype map(Datatype keyType, Datatype valueType) {
    return mapDatatype(DataTypeName.Map, keyType, valueType);
  }
}
