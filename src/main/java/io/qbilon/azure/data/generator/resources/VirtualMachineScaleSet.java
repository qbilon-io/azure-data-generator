package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.VirtualMachineScaleSetConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class VirtualMachineScaleSet implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_VIRTUAL_MACHINE_SCALE_SET;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_VIRTUAL_MACHINE_SCALE_SET,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_AKS_MANAGED_COORDINATION),
            azureTag(TAG_AKS_MANAGED_CREATE_OPERATION_ID),
            azureTag(TAG_AKS_MANAGED_CREATION_SOURCE),
            azureTag(TAG_AKS_MANAGED_KUBELET_IDENTITY_CLIENT_ID),
            azureTag(TAG_AKS_MANAGED_ORCHESTRATOR),
            azureTag(TAG_AKS_MANAGED_POOL_NAME),
            azureTag(TAG_AKS_MANAGED_RESOURCE_NAME_SUFFIX),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(CAPACITY, AzureDataHelper.randomInt(500)),
            property(COMPUTER_NAME_PREFIX, "Computer Name Prefix"),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("VM-Scale-Set")),
            property(OS_TYPE, AzureDataHelper.randomChoose("Windows", "Linux")),
            property(SKU, SKU),
            property(TYPE, "VM Scale Set")
        );
    }
}
