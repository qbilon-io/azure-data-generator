package io.qbilon.azure.data.generator.resources;

import io.qbilon.azure.data.generator.data.input.Node;

public interface AzureNodeGenerator {

    String type();

    Node generate();

}
