package io.qbilon.azure.data.generator.constants.azure.common;

public class SQLCommonConstants {
  public static final String ANY_IP_ACCESS_ALLOWED = "Any IP Access Allowed";
  public static final String AUTOGROW_STORAGE = "Autogrow Storage";
  public static final String CREATED_AT = "Created at";
  public static final String DOMAIN_NAME = "Domain Name";
  public static final String EARLIEST_RESTORE_DATE = "Earliest Restore Date";
  public static final String HIGH_AVAILABILITY_MODE = "High Availability Mode";
  public static final String LOCATION = "Location";
  public static final String NAME = "Name";
  public static final String PUBLIC_NETWORK_ACCESS_ALLOWED = "Public Network Access Allowed";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String SKU_CAPACITY = "SKU Capacity";
  public static final String SKU_NAME = "SKU Name";
  public static final String SKU_SIZE = "SKU Size";
  public static final String SSL_ENFORCEMENT_STATE = "SSL Enforcement State";
  public static final String STORAGE_GB = "Storage (GB)";
  public static final String STORAGE_MB = "Storage (MB)";
  public static final String TYPE = "Type";
  public static final String VERSION = "Version";
}
