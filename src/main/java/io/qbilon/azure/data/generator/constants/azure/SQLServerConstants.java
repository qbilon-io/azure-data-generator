package io.qbilon.azure.data.generator.constants.azure;

public class SQLServerConstants {
  public static final String RESOURCE_NAME_SQL_SERVER = "Azure SQL Server";
  public static final String RESOURCE_NAME_SQL_DATABASE = "Azure SQL Database";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String DOMAIN_NAME = "Domain Name";
  public static final String PUBLIC_NETWORK_ACCESS_ALLOWED = "Public Network Access Allowed";
  public static final String ANY_IP_ACCESS_ALLOWED = "Any IP Access Allowed";
  public static final String TYPE = "Type";
  public static final String VERSION = "Version";
  public static final String COLLATION = "Collation";
  public static final String CREATED_AT = "Created at";
  public static final String SERVICE_LEVEL_OBJECTIVE_NAME = "Service Level Objective Name";
  public static final String DATABASE_EDITION = "Database Edition";
  public static final String THREAT_DETECTION_POLICY = "Threat Detection Policy";
  public static final String IS_DATA_WAREHOUSE = "Is Data Warehouse";
  public static final String TRANSPARENT_DATA_ENCRYPTION = "Transparent Data Encryption";
  public static final String MAX_SIZE_BYTES = "Max. Size (Bytes)";
  public static final String STATUS = "Status";
}
