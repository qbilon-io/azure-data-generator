package io.qbilon.azure.data.generator.constants.azure;

public class GeneralAzureConstants {
  public static final String AZURE_ID = "id";
  public static final String AZURE_NAME = "Name";
}
