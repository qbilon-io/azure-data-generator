package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.constants.azure.VirtualMachineConstants.NAME;
import static io.qbilon.azure.data.generator.constants.azure.VirtualMachineConstants.PUBLIC_IP_ADDRESS;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class PublicIPAddress implements AzureNodeGenerator {
    @Override
    public String type() {
        return PUBLIC_IP_ADDRESS;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            PUBLIC_IP_ADDRESS,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_AKS_MANAGED_CLUSTER_NAME),
            azureTag(TAG_AKS_MANAGED_CLUSTER_RG),
            azureTag(TAG_AKS_MANAGED_TYPE),
            azureTag(TAG_K8S_AZURE_CLUSTER_NAME),
            azureTag(TAG_K8S_AZURE_SERVICE),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_PROJECT),
            azureTag(TAG_SOLUTION),
            azureTag(TAG_TEAM),
            azureTag(TAG_USER),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("Public-IP-Address")),
            property(PUBLIC_IP_ADDRESS, AzureDataHelper.randomIP()),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE)
        );
    }
}
