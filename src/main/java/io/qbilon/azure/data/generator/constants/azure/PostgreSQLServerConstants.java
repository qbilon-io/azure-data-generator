package io.qbilon.azure.data.generator.constants.azure;

public class PostgreSQLServerConstants {

  public static final String RESOURCE_NAME_POSTGRESQL_SERVER = "Azure PostgreSQL Server";
}
