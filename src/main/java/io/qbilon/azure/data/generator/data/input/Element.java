package io.qbilon.azure.data.generator.data.input;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Element {
  /** The datasource-specific unique ID of this graph element */
  @JsonProperty("_key")
  public String id;
  /** The type name of this graph element, e.g., 'uses' for an Edge or 'Server' for a node */
  public String type;
  /**
   * The name of the attribute whose value should be used as name for this graph element, e.g.,
   * 'Hostname', if this graph element has a property with name 'Hostname' then the value of this
   * property will be used as name
   */
  public String nameAttribute;
  /** A collection of properties this graph element has */
  public Map<String, Property> properties = new HashMap<>();
  /**
   * Describes the seen timestamp the node got discovered by the adapter Must be set before
   * upserting into the graph
   */
  public LocalDateTime creationTime;

  public static List<Property> properties(Property... properties) {
    return Arrays.stream(properties).filter(Objects::nonNull).collect(Collectors.toList());
  }

  public void add(Property property) {
    this.properties.put(property.name, property);
  }
}
