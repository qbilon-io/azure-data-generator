package io.qbilon.azure.data.generator.constants.azure;

public class ApiManagementConstants {
  public static final String RESOURCE_NAME_API_MANAGEMENT_SERVICE = "API Management Service";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String MANAGEMENT_API_URL = "Management Api Url";
  public static final String GATEWAY_URL = "Gateway Url";
  public static final String NAME = "Name";
  public static final String SKU = "Sku";
  public static final String SKU_CAPACITY = "Sku Capacity";
}
