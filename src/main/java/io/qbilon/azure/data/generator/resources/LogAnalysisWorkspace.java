package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.LogAnalyticsConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class LogAnalysisWorkspace implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_NAME_LOG_ANALYTICS_WORKSPACE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_LOG_ANALYTICS_WORKSPACE,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            k8sLabel(TAG_COST_CENTER),
            k8sLabel(TAG_CREATED),
            k8sLabel(TAG_PRODUCT),
            k8sLabel(TAG_SERVICE),
            k8sLabel(TAG_TEAM),
            k8sLabel(TAG_TEAMS),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(LOCATION, "GER"),
            property(NAME, AzureDataHelper.randomName("Log-Analysis-Workspace")),
            property(PROVISIONING_STATE, PROVISIONING_STATE),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(RETENTION_IN_DAYS, AzureDataHelper.randomInt(300)),
            property(SKU, SKU),
            property(TYPE, "Log Analysis Workspace")
        );
    }
}
