package io.qbilon.azure.data.generator;

import static io.qbilon.azure.data.generator.data.input.Property.property;

import io.qbilon.azure.data.generator.data.input.Property;

public class Constants {

    public static Property azureTag(String tag) {
        String AZURE_TAG = "azure-tag-";
        return property(AZURE_TAG + tag, AZURE_TAG + tag);
    }

    public static final String TAG_AKS_MANAGED_CLUSTER_NAME = "aks-managed-cluster-name";
    public static final String TAG_AKS_MANAGED_CLUSTER_RG = "aks-managed-cluster-rg";
    public static final String TAG_AKS_MANAGED_COORDINATION = "aks-managed-coordination";
    public static final String TAG_AKS_MANAGED_CREATE_OPERATION_ID = "aks-managed-createOperationID";
    public static final String TAG_AKS_MANAGED_CREATION_SOURCE = "aks-managed-creationSource";
    public static final String TAG_AKS_MANAGED_KUBELET_IDENTITY_CLIENT_ID = "aks-managed-kubeletIdentityClientID";
    public static final String TAG_AKS_MANAGED_ORCHESTRATOR = "aks-managed-orchestrator";
    public static final String TAG_AKS_MANAGED_POOL_NAME = "aks-managed-poolName";
    public static final String TAG_AKS_MANAGED_RESOURCE_NAME_SUFFIX = "aks-managed-resourceNameSuffix";
    public static final String TAG_AKS_MANAGED_TYPE = "aks-managed-type";
    public static final String TAG_APP_SENSOR = "AppSensor";
    public static final String TAG_COSMOS_ACCOUNT_TYPE = "CosmosAccountType";
    public static final String TAG_COST_CENTER = "CostCenter";
    public static final String TAG_CREATED = "Created";
    public static final String TAG_CREATED_BY = "Created By";
    public static final String TAG_CREATION_TIME = "CreationTime";
    public static final String TAG_DEFAULT_EXPERIENCE = "defaultExperience";
    public static final String TAG_DISPLAY_NAME = "displayName";
    public static final String TAG_ENVIRONMENT = "environment";
    public static final String TAG_HIDDEN_COSMOS_MM_SPECIAL = "hidden-cosmos-mmspecial";
    public static final String TAG_INFORMATION_CLASSIFICATION = "InformationClassification";
    public static final String TAG_INGRESS_FOR_AKS_CLUSTER_ID = "ingress-for-aks-cluster-id";
    public static final String TAG_K8S_AZURE_CREATED_BY = "k8s-azure-created-by";
    public static final String TAG_K8S_AZURE_CLUSTER_NAME = "k8s-azure-cluster-name";
    public static final String TAG_K8S_AZURE_SERVICE = "k8s-azure-service";
    public static final String TAG_KUBERNETES_IO_CREATED_FOR_PV_NAME = "kubernetes.io-created-for-pv-name";
    public static final String TAG_KUBERNETES_IO_CREATED_FOR_PVC_NAME = "kubernetes.io-created-for-pvc-name";
    public static final String TAG_KUBERNETES_IO_CREATED_FOR_PVC_NAMESPACE = "kubernetes.io-created-for-pvc-namespace";
    public static final String TAG_MS_RESOURCE_USAGE = "ms-resource-tag";
    public static final String TAG_MANAGED_BY_K8S_INGRESS = "managed-by-k8s-ingress";
    public static final String TAG_PRODUCT = "Product";
    public static final String TAG_PROJECT = "Project";
    public static final String TAG_SERVICE = "Service";
    public static final String TAG_STAGE = "Stage";
    public static final String TAG_SET_CONTRIBUTOR = "SetContributor";
    public static final String TAG_SOLUTION = "Solution";
    public static final String TAG_TEAM = "Team";
    public static final String TAG_TEAMS = "Teams";
    public static final String TAG_OWNER = "Owner";
    public static final String TAG_USER = "User";
    public static final String TAG_UPDATED_BY_SCRIPT = "updated-by-script";

    public static Property k8sLabel(String label) {
        String K8S_LABEL = "k8s-label-";
        return property(K8S_LABEL + label, K8S_LABEL + label);
    }

    public static final String LABEL_ADDON_MANAGER_KUBERNETES_IO_MODE = "addonmanager.kubernetes.io/mode";
    public static final String LABEL_ADMISSION_GATEKEEPER_SH_IGNORE = "addonmanager.gatekeeper.sh/ignore";
    public static final String LABEL_APP = "app";
    public static final String LABEL_APP_KUBERNETES_IO_COMPONENT = "app.kubernetes.io/component";
    public static final String LABEL_APP_KUBERNETES_IO_INSTANCE = "app.kubernetes.io/instance";
    public static final String LABEL_APP_KUBERNETES_IO_MANAGED_BY = "app.kubernetes.io/managed-by";
    public static final String LABEL_APP_KUBERNETES_IO_NAME = "app.kubernetes.io/name";
    public static final String LABEL_APP_KUBERNETES_IO_PART_OF = "app.kubernetes.io/part-of";
    public static final String LABEL_APP_KUBERNETES_IO_VERSION = "app.kubernetes.io/version";
    public static final String LABEL_APPMANAGER = "appmanager";
    public static final String LABEL_APPPUBLIC = "apppublic";
    public static final String LABEL_APPREPLICATOR = "appreplicator";
    public static final String LABEL_AZURE_WORKLOAD_IDENTITY_IO_SYSTEM = "azure-workload-identity.io/system";
    public static final String LABEL_AZURE_WORKLOAD_IDENTITY_USE = "azure-workload-identity/use";
    public static final String LABEL_CONTROL_PLANE = "control-plane";
    public static final String LABEL_CHART = "chart";
    public static final String LABEL_COMPONENT = "component";
    public static final String LABEL_CONTROLLER_REVISION_HASH = "controller-revision-hash";
    public static final String LABEL_CONTROLLER_UID = "component-uid";
    public static final String LABEL_ENVIRONMENT = "environment";
    public static final String LABEL_GATEKEEPER_SH_OPERATION = "gatekeeper.sh/operation";
    public static final String LABEL_GATEKEEPER_SH_SYSTEM = "gatekeeper.sh/system";
    public static final String LABEL_HELM_SH_CHART = "helm.sh/chart";
    public static final String LABEL_HELM_TOOLKIT_FLUXCD_IO_NAME = "helm.toolkit.fluxcd.io/name";
    public static final String LABEL_HELM_TOOLKIT_FLUXCD_IO_NAMESPACE = "helm.toolkit.fluxcd.io/namespace";
    public static final String LABEL_HERITAGE = "heritage";
    public static final String LABEL_JOB_NAME = "job-name";
    public static final String LABEL_JOB_LABEL = "jobLabel";
    public static final String LABEL_K8S_APP = "k8s-app";
    public static final String LABEL_KUBERNETES_AZURE_COM_MANAGED_BY = "kubernetes.azure.com/managed-by";
    public static final String LABEL_KUBERNETES_IO_CLUSTER_SERVICE = "kubernetes.io/cluster-service";
    public static final String LABEL_KUBERNETES_IO_METADATA_NAME = "kubernetes.io/metadata.name";
    public static final String LABEL_KUBERNETES_IO_NAME = "kubernetes.io/name";
    public static final String LABEL_KUSTOMIZE_TOOLKIT_FLUXCD_IO_NAME = "kustomize.toolkit.fluxcd.io/name";
    public static final String LABEL_NAME = "name";
    public static final String LABEL_POD_TEMPLATE_GENERATION = "pod-template-generation";
    public static final String LABEL_POD_TEMPLATE_HASH = "pod-template-hash";
    public static final String LABEL_PROMETHEUS = "prometheus";
    public static final String LABEL_RELEASE = "release";
    public static final String LABEL_RS_NAME = "rsName";
    public static final String LABEL_SERVICE = "service";
    public static final String LABEL_SUFFIX = "suffix";
    public static final String LABEL_SOLUTION = "solution";
    public static final String LABEL_TIER = "tier";
    public static final String LABEL_VERSION = "version";


}
