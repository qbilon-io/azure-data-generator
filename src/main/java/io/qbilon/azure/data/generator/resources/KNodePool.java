package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.KubernetesClusterConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class KNodePool implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_NAME_NODE_POOL;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_NODE_POOL,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(AGENT_POOL_MODE, AGENT_POOL_MODE),
            property(AUTOSCALING_ENABLED, AzureDataHelper.randomBool()),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(KUBELET_DISK_TYPE, "SSD"),
            property(MAX_NODE_SIZE, AzureDataHelper.randomInt(10_000)),
            property(NAME, AzureDataHelper.randomName("Kubernetes-Node-Pool")),
            property(NUMBER_NODES, AzureDataHelper.randomInt(100)),
            property(OS_DISK_SIZE_GB, AzureDataHelper.randomInt(1_000)),
            property(OS_TYPE, AzureDataHelper.randomChoose("Windows", "Linux")),
            property(POWER_STATE, "running"),
            property(TYPE, "NodePool"),
            property(VM_SIZE, AzureDataHelper.randomInt(10_000))
        );
    }
}
