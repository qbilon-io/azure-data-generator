package io.qbilon.azure.data.generator.data.datatype;

public enum DataTypeName {
  Text(true),
  Number(true),
  Boolean(true),
  DateTime(true),
  List(false),
  Map(false);

  private final boolean isPrimitive;

  DataTypeName(boolean isPrimitive) {
    this.isPrimitive = isPrimitive;
  }

  public boolean isPrimitive() {
    return isPrimitive;
  }
}
