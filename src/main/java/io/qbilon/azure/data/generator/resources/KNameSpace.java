package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sCommonConstants.ID;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sCommonConstants.NAME;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sNamespaceConstants.RESOURCE_ID_NAMESPACE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class KNameSpace implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_ID_NAMESPACE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_ID_NAMESPACE,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(ID, AzureDataHelper.randomId()),
            k8sLabel(LABEL_ADDON_MANAGER_KUBERNETES_IO_MODE),
            k8sLabel(LABEL_ADMISSION_GATEKEEPER_SH_IGNORE),
            k8sLabel(LABEL_APP_KUBERNETES_IO_INSTANCE),
            k8sLabel(LABEL_APP_KUBERNETES_IO_PART_OF),
            k8sLabel(LABEL_APP_KUBERNETES_IO_VERSION),
            k8sLabel(LABEL_CONTROL_PLANE),
            k8sLabel(LABEL_GATEKEEPER_SH_SYSTEM),
            k8sLabel(LABEL_KUBERNETES_IO_CLUSTER_SERVICE),
            k8sLabel(LABEL_KUBERNETES_IO_METADATA_NAME),
            k8sLabel(LABEL_NAME),
            property(NAME, AzureDataHelper.randomName("Kubernetes-Namespace"))
        );
    }
}
