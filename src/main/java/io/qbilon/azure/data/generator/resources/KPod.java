package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sCommonConstants.*;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sPodConstants.RESOURCE_ID_POD;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class KPod implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_ID_POD;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_ID_POD,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            property(CPU_USAGE, AzureDataHelper.randomInt(100)),
            property(ID, AzureDataHelper.randomId()),
            k8sLabel(LABEL_APP),
            k8sLabel(LABEL_APP_KUBERNETES_IO_COMPONENT),
            k8sLabel(LABEL_APP_KUBERNETES_IO_INSTANCE),
            k8sLabel(LABEL_APP_KUBERNETES_IO_MANAGED_BY),
            k8sLabel(LABEL_APP_KUBERNETES_IO_NAME),
            k8sLabel(LABEL_APP_KUBERNETES_IO_PART_OF),
            k8sLabel(LABEL_APP_KUBERNETES_IO_VERSION),
            k8sLabel(LABEL_APPMANAGER),
            k8sLabel(LABEL_APPPUBLIC),
            k8sLabel(LABEL_APPREPLICATOR),
            k8sLabel(LABEL_AZURE_WORKLOAD_IDENTITY_IO_SYSTEM),
            k8sLabel(LABEL_AZURE_WORKLOAD_IDENTITY_USE),
            k8sLabel(LABEL_CHART),
            k8sLabel(LABEL_COMPONENT),
            k8sLabel(LABEL_CONTROL_PLANE),
            k8sLabel(LABEL_CONTROLLER_REVISION_HASH),
            k8sLabel(LABEL_CONTROLLER_UID),
            k8sLabel(LABEL_ENVIRONMENT),
            k8sLabel(LABEL_GATEKEEPER_SH_OPERATION),
            k8sLabel(LABEL_GATEKEEPER_SH_SYSTEM),
            k8sLabel(LABEL_HELM_SH_CHART),
            k8sLabel(LABEL_HERITAGE),
            k8sLabel(LABEL_JOB_NAME),
            k8sLabel(LABEL_JOB_LABEL),
            k8sLabel(LABEL_K8S_APP),
            k8sLabel(LABEL_KUBERNETES_AZURE_COM_MANAGED_BY),
            k8sLabel(LABEL_KUBERNETES_IO_CLUSTER_SERVICE),
            k8sLabel(LABEL_NAME),
            k8sLabel(LABEL_POD_TEMPLATE_GENERATION),
            k8sLabel(LABEL_POD_TEMPLATE_HASH),
            k8sLabel(LABEL_PROMETHEUS),
            k8sLabel(LABEL_RELEASE),
            k8sLabel(LABEL_RS_NAME),
            k8sLabel(LABEL_SOLUTION),
            k8sLabel(LABEL_TIER),
            k8sLabel(LABEL_VERSION),
            property(MEMORY_USAGE, AzureDataHelper.randomDouble(100)),
            property(NAME, AzureDataHelper.randomName("Kubernetes-Pod"))
        );
    }
}
