package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.constants.kubernetes.K8sCommonConstants.NAME;
import static io.qbilon.azure.data.generator.constants.kubernetes.K8sVolumeConstants.RESOURCE_ID_VOLUME;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;

public class KVolume implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_ID_VOLUME;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_ID_VOLUME,
            NAME,
            property(NAME, AzureDataHelper.randomName("Kubernetes-Volume"))
        );
    }
}
