package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.AppServiceConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class AppServicePlan implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_NAME_APP_SERVICE_PLAN;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_APP_SERVICE_PLAN,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_DISPLAY_NAME),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_SERVICE),
            azureTag(TAG_TEAM),
            azureTag(TAG_TEAMS),
            property(CAPACITY, AzureDataHelper.randomInt(100_000)),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(MAX_INSTANCES, AzureDataHelper.randomInt(5)),
            property(NAME, AzureDataHelper.randomName("ASP")),
            property(NUMBER_OF_WEB_APPS, AzureDataHelper.randomInt(5)),
            property(OPERATING_SYSTEM, AzureDataHelper.randomChoose("LINUX", "WINDOWS")),
            property(PER_SITE_SCALING, AzureDataHelper.randomBool()),
            property(PRICING_TIER_SKU, AzureDataHelper.randomChoose("F1", "Y1")),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE)
        );
    }
}
