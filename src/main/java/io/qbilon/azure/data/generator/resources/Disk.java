package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.DiskConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class Disk implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_DISK;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_DISK,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_K8S_AZURE_CREATED_BY),
            azureTag(TAG_KUBERNETES_IO_CREATED_FOR_PV_NAME),
            azureTag(TAG_KUBERNETES_IO_CREATED_FOR_PVC_NAME),
            azureTag(TAG_KUBERNETES_IO_CREATED_FOR_PVC_NAMESPACE),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_PROJECT),
            azureTag(TAG_SOLUTION),
            azureTag(TAG_TEAM),
            azureTag(TAG_USER),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(ENCRYPTION_ENABLED, AzureDataHelper.randomBool()),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(MAX_DISK_IOPS_READ_WRITE, AzureDataHelper.randomInt(1_000)),
            property(NAME, AzureDataHelper.randomName("Disk")),
            property(OS_TYPE, AzureDataHelper.randomChoose("LINUX", "WINDOWS")),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(SIZE_GB, AzureDataHelper.randomInt(100)),
            property(SKU, AzureDataHelper.randomChoose("Standard_LRS", "StandardSSD_LRS")),
            property(TIME_CREATED, AzureDataHelper.randomDate())
        );
    }
}
