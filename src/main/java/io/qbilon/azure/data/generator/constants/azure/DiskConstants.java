package io.qbilon.azure.data.generator.constants.azure;

public class DiskConstants {
  public static final String RESOURCE_NAME_DISK = "Azure Disk";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String RELATION_NAME_HAS_ATTACHED = "has attached";
  public static final String NAME = "Name";
  public static final String SIZE_GB = "Size (GB)";
  public static final String ENCRYPTION_ENABLED = "Encryption Enabled";
  public static final String OS_TYPE = "OS-Type";
  public static final String SKU = "Sku";
  public static final String TIME_CREATED = "Time Created";
  public static final String MAX_DISK_IOPS_READ_ONLY = "Max. Disk Iops ReadOnly";
  public static final String MAX_DISK_IOPS_READ_WRITE = "Max. Disk Iops Read/Write";
}
