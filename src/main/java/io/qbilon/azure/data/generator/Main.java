//DEPS com.fasterxml.jackson.core:jackson-databind:2.15.0
//DEPS info.picocli:picocli:4.7.1
//DEPS com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.15.0

//SOURCES constants/azure/ApiManagementConstants.java
//SOURCES constants/azure/ApplicationGatewayConstants.java
//SOURCES constants/azure/AppServiceConstants.java
//SOURCES constants/azure/CosmosDBConstants.java
//SOURCES constants/azure/CostConstants.java
//SOURCES constants/azure/DiskConstants.java
//SOURCES constants/azure/FrontDoorConstants.java
//SOURCES constants/azure/GeneralAzureConstants.java
//SOURCES constants/azure/KubernetesClusterConstants.java
//SOURCES constants/azure/LoadBalancerConstants.java
//SOURCES constants/azure/LogAnalyticsConstants.java
//SOURCES constants/azure/MySQLFlexibleServerConstants.java
//SOURCES constants/azure/MySQLServerConstants.java
//SOURCES constants/azure/NetworkConstants.java
//SOURCES constants/azure/NetworkInterfaceConstants.java
//SOURCES constants/azure/NetworkInterfaceConstants.java
//SOURCES constants/azure/NetworkSecurityGroupConstants.java
//SOURCES constants/azure/PostgreSQLFlexibleServerConstants.java
//SOURCES constants/azure/PostgreSQLServerConstants.java
//SOURCES constants/azure/NetworkConstants.java
//SOURCES constants/azure/PrivateEndpointConstants.java
//SOURCES constants/azure/ResourceGroupConstants.java
//SOURCES constants/azure/SQLServerConstants.java
//SOURCES constants/azure/StorageAccountConstants.java
//SOURCES constants/azure/SubscriptionConstants.java
//SOURCES constants/azure/TrafficManagerConstants.java
//SOURCES constants/azure/VirtualMachineConstants.java
//SOURCES constants/azure/VirtualMachineScaleSetConstants.java

//SOURCES constants/azure/common/SQLCommonConstants.java

//SOURCES constants/kubernetes/K8sCommonConstants.java
//SOURCES constants/kubernetes/K8sContainerConstants.java
//SOURCES constants/kubernetes/K8sDeploymentConstants.java
//SOURCES constants/kubernetes/K8sNamespaceConstants.java
//SOURCES constants/kubernetes/K8sPersistentVolumeConstants.java
//SOURCES constants/kubernetes/K8sPodConstants.java
//SOURCES constants/kubernetes/K8sVolumeConstants.java

//SOURCES data/datatype/DataSuperTypeName.java
//SOURCES data/datatype/Datatype.java
//SOURCES data/datatype/DatatypeDeserializer.java
//SOURCES data/datatype/DataTypeName.java
//SOURCES data/datatype/DatatypeSerializer.java

//SOURCES data/exception/InvalidDatatypeException.java

//SOURCES data/input/Edge.java
//SOURCES data/input/Element.java
//SOURCES data/input/Graph.java
//SOURCES data/input/Node.java
//SOURCES data/input/Property.java

//SOURCES resources/APIManagementService.java
//SOURCES resources/AppService.java
//SOURCES resources/AppServicePlan.java
//SOURCES resources/AzureNodeGenerator.java
//SOURCES resources/BlobContainer.java
//SOURCES resources/CosmosDBAccount.java
//SOURCES resources/CosmosDBDatabase.java
//SOURCES resources/Disk.java
//SOURCES resources/FrontDoor.java
//SOURCES resources/KCluster.java
//SOURCES resources/KContainer.java
//SOURCES resources/KDeployment.java
//SOURCES resources/KImage.java
//SOURCES resources/KNameSpace.java
//SOURCES resources/KNodePool.java
//SOURCES resources/KPod.java
//SOURCES resources/KVolume.java
//SOURCES resources/LoadBalancer.java
//SOURCES resources/LogAnalysisWorkspace.java
//SOURCES resources/Network.java
//SOURCES resources/NetworkInterface.java
//SOURCES resources/NetworkSecurityGroup.java
//SOURCES resources/PostgreSQLFlexibleServer.java
//SOURCES resources/PostgreSQLServer.java
//SOURCES resources/PrivateEndpoint.java
//SOURCES resources/PublicIPAddress.java
//SOURCES resources/Region.java
//SOURCES resources/ResourceGroup.java
//SOURCES resources/SQLDatabase.java
//SOURCES resources/SQLServer.java
//SOURCES resources/StorageAccount.java
//SOURCES resources/Subscription.java
//SOURCES resources/TrafficManager.java
//SOURCES resources/VirtualGateway.java
//SOURCES resources/VirtualMachine.java
//SOURCES resources/VirtualMachineScaleSet.java

//SOURCES AzureDataGenerator.java
//SOURCES AzureDataHelper.java
//SOURCES Constants.java

package io.qbilon.azure.data.generator;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.qbilon.azure.data.generator.data.input.Edge;
import io.qbilon.azure.data.generator.data.input.Graph;
import io.qbilon.azure.data.generator.data.input.Node;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "generate", mixinStandardHelpOptions = true, version = "azure-generator 1.0", description = "Creates an artificial Azure IT Landscape")
public class Main implements Callable<Integer>{

    @Option(names = { "-s", "--size" }, description = "Size of the graph, 1 ~= 12k nodes and edges, 100 ~= 1.200k nodes and edges")
    private int size;

    @Override
    public Integer call() {
        System.out.println("Generating graph with size factor: " + size);
        try {
            AzureDataGenerator gen = new AzureDataGenerator();
            Graph g = gen.graphSize(size).generate();

            System.out.println("Finished graph generation.");
            System.out.println("Generated " + g.nodes.size() + " Nodes");
            System.out.println("Generated " + g.edges.size() + " Edges");
            System.out.println("Writing graph to files...");

            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));

            JsonMapper mapper = new JsonMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            // StdDateFormat is ISO8601 since jackson 2.9
            mapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));

            final File nodeFile = new File("nodes.ldjson");
            try (SequenceWriter seq = mapper.writer()
                .withRootValueSeparator("\n") // Important! Default value separator is single space
                .writeValues(nodeFile)) {
                    for (Node node : g.nodes) {
                        seq.write(node);
                    }
                    seq.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }

            final File edgeFile = new File("edges.ldjson");
            try (SequenceWriter seq = mapper.writer()
                .withRootValueSeparator("\n") // Important! Default value separator is single space
                .writeValues(edgeFile)) {
                    for (Edge edge : g.edges) {
                        seq.write(edge);
                    }
                    seq.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Finished writing.");

        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    public static void main(String[] args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }
}