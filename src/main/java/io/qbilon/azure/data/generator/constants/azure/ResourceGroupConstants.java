package io.qbilon.azure.data.generator.constants.azure;

public class ResourceGroupConstants {
  public static final String RESOURCE_NAME_RESOURCE_GROUP = "Azure Resource Group";
  public static final String RESOURCE_NAME_REGION = "Azure Region";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String PROVISIONING_STATE = "Provisioning State";
  public static final String RESOURCE_GROUP_ATTRIBUTE = "Resource Group Name";
}
