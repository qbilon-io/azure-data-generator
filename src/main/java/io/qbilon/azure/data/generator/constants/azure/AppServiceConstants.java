package io.qbilon.azure.data.generator.constants.azure;

public class AppServiceConstants {
  public static final String RESOURCE_NAME_APP_SERVICE = "Azure App Service";
  public static final String RESOURCE_NAME_APP_SERVICE_PLAN = "Azure App Service Plan";

  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String RELATION_NAME_MANAGES = "manages";

  public static final String NAME = "Name";
  public static final String AVERAGE_CPU_SECONDS_7_D = "Average CPU (Seconds/7d)";
  public static final String AVAILABILITY_STATE = "Availability State";
  public static final String CLIENT_AFFINITY_ENABLED = "Client Affinity Enabled";
  public static final String CLIENT_CERT_ENABLED = "Client Cert Enabled";
  public static final String CONTAINER_SIZE = "Container Size";
  public static final String DAILY_MEMORY_TIME_QUOTA = "Daily Memory Time Quota";
  public static final String DEFAULT_HOSTNAME = "Default Hostname";
  public static final String HTTPS_ONLY = "Https Only";
  public static final String HYPERV_ENABLED = "HyperV Enabled";
  public static final String LAST_MODIFIED_TIME = "Last Modified Time";
  public static final String OPERATING_SYSTEM = "Operating System";
  public static final String MAX_NUMBER_OF_WORKERS = "Max Number Of Workers";
  public static final String STATE = "State";
  public static final String CAPACITY = "Capacity";
  public static final String MAX_INSTANCES = "Max. Instances";
  public static final String NUMBER_OF_WEB_APPS = "Number Of WebApps";
  public static final String PER_SITE_SCALING = "Per Site Scaling";
  public static final String PRICING_TIER_SKU = "PricingTier Sku";
}
