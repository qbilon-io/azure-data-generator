package io.qbilon.azure.data.generator;

import io.qbilon.azure.data.generator.data.input.Edge;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.resources.*;

import static io.qbilon.azure.data.generator.data.input.Edge.edge;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.*;
import java.util.stream.Stream;

public class AzureDataHelper {

    public static final String API_MANAGEMENT_SERVICE = new APIManagementService().type();
    public static final String APP_SERVICE = new AppService().type();
    public static final String APP_SERVICE_PLAN = new AppServicePlan().type();
    public static final String BLOB_CONTAINER = new BlobContainer().type();
    public static final String COSMOS_DB_ACCOUNT = new CosmosDBAccount().type();
    public static final String COSMOS_DB_DATABASE = new CosmosDBDatabase().type();
    public static final String DISK = new Disk().type();
    public static final String FRONT_DOOR = new FrontDoor().type();
    public static final String K_CLUSTER = new KCluster().type();
    public static final String K_CONTAINER = new KContainer().type();
    public static final String K_DEPLOYMENT = new KDeployment().type();
    public static final String K_IMAGE = new KImage().type();
    public static final String K_NAME_SPACE = new KNameSpace().type();
    public static final String K_NODE_POOL = new KNodePool().type();
    public static final String K_POD = new KPod().type();
    public static final String K_VOLUME = new KVolume().type();
    public static final String LOAD_BALANCER = new LoadBalancer().type();
    public static final String LOG_ANALYSIS_WORKSPACE = new LogAnalysisWorkspace().type();
    public static final String NETWORK = new Network().type();
    public static final String NETWORK_INTERFACE = new NetworkInterface().type();
    public static final String NETWORK_SECURITY_GROUP = new NetworkSecurityGroup().type();
    public static final String POSTGRE_SQL_FLEXIBLE_SERVER = new PostgreSQLFlexibleServer().type();
    public static final String POSTGRE_SQL_SERVER = new PostgreSQLServer().type();
    public static final String PRIVATE_ENDPOINT = new PrivateEndpoint().type();
    public static final String PUBLIC_IP_ADDRESS = new PublicIPAddress().type();
    public static final String REGION = new Region().type();
    public static final String RESOURCE_GROUP = new ResourceGroup().type();
    public static final String SQL_DATABASE = new SQLDatabase().type();
    public static final String SQL_SERVER = new SQLServer().type();
    public static final String STORAGE_ACCOUNT = new StorageAccount().type();
    public static final String SUBSCRIPTION = new Subscription().type();
    public static final String TRAFFIC_MANAGER = new TrafficManager().type();
    public static final String VIRTUAL_GATEWAY = new VirtualGateway().type();
    public static final String VIRTUAL_MACHINE = new VirtualMachine().type();
    public static final String VIRTUAL_MACHINE_SCALE_SET = new VirtualMachineScaleSet().type();

    public static final String CONTAINS = "contains";
    public static final String NAME = "name";

    private static final Random random = new Random();

    private AzureDataHelper() {
    }

    public static double randomDouble(double upperBound) {
        return random.nextDouble() * upperBound;
    }

    public static int randomInt(int upperBound) {
        return random.nextInt(upperBound);
    }

    public static boolean randomBool() {
        return random.nextInt(2) == 0;
    }

    public static String randomDate() {
        String day = String.valueOf(random.nextInt(29));
        String month = String.valueOf(random.nextInt(13));

        return day + "." + month + ".2022 00:00";
    }

    public static String randomName(String prefix) {
        return prefix + "-" + random.nextInt(100000000);
    }

    public static String randomId() {
        return UUID.randomUUID().toString();
    }

    public static String randomIP() {
        return
            random.nextInt(129) + "." +
                random.nextInt(129) + "." +
                random.nextInt(129) + "." +
                random.nextInt(129);
    }

    public static <T> T randomChoose(T valA, T valB) {
        return random.nextInt(2) == 0 ? valA : valB;
    }

    public static Edge defaultContainsEdge(Node targetNode, List<Node> sourceNodes) {
        return defaultEdge(sourceNodes.get(AzureDataHelper.randomInt(sourceNodes.size())).id, targetNode.id, AzureDataHelper.CONTAINS);
    }

    public static Edge defaultEdge(Node targetNode, List<Node> sourceNodes, String name) {
        return defaultEdge(sourceNodes.get(AzureDataHelper.randomInt(sourceNodes.size())).id, targetNode.id, name);
    }

    public static Edge defaultContainsEdge(String sourceNodeId, String targetNodeId) {
        return defaultEdge(sourceNodeId, targetNodeId, AzureDataHelper.CONTAINS);
    }

    public static Edge defaultEdge(String sourceNodeId, String targetNodeId, String name) {
        return edge(
            UUID.randomUUID().toString(),
            name,
            AzureDataHelper.NAME,
            sourceNodeId,
            targetNodeId,
            property(AzureDataHelper.NAME, name)
        );
    }

    public static void createTransitiveEdges(Edges edges, Set<String> sourceIds, Set<String> middleNodeIds) {
        List<Edge> newEdges = new ArrayList<>();
        sourceIds.forEach(id -> {
            Stream<String> reachableNetworks = edges.targets(id).stream().filter(middleNodeIds::contains);
            reachableNetworks.forEach(nId ->
                edges.targets(nId).forEach(t -> newEdges.add(AzureDataHelper.defaultContainsEdge(id, t)))
            );
        });
        newEdges.forEach(edges::add);
    }

    public static class Edges {
        private final Map<String, Set<String>> sourceTargetMap = new HashMap<>();
        private final List<Edge> edges = new ArrayList<>();

        public void add(Edge edge) {
            edges.add(edge);
            sourceTargetMap.computeIfAbsent(edge.sourceId, e -> new HashSet<>()).add(edge.targetId);
        }

        public List<Edge> edges() {
            return this.edges;
        }

        public Set<String> targets(String source) {
            return this.sourceTargetMap.getOrDefault(source, Set.of());
        }
    }
}
