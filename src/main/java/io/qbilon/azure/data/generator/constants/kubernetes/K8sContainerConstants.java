package io.qbilon.azure.data.generator.constants.kubernetes;

public class K8sContainerConstants {
  public static final String RESOURCE_ID_CONTAINER = "K8s Container";
  public static final String IMAGE = "Image";
  public static final String ARGS = "Arguments";
  public static final String IMAGE_PULL_POLICY = "Image Pull Policy";
  public static final String RESOURCE_ID_IMAGE = "K8s Image";
  public static final String RELATION_REFERS_TO = "refers to";
  public static final String REQUEST_RULE_RAM = "Request Rule RAM";
  public static final String REQUEST_RULE_CPU = "Request Rule CPU";
  public static final String LIMIT_RULE_RAM = "Limit Rule RAM";
  public static final String LIMIT_RULE_CPU = "Limit Rule CPU";
}
