package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.KubernetesClusterConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.NAME;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class KCluster implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_NAME_KUBERNETES_CLUSTER;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_KUBERNETES_CLUSTER,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(DNS_PREFIX, "0.0.0.0"),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("Kubernetes-Cluster")),
            property(POWER_STATE, "running"),
            property(VERSION, AzureDataHelper.randomDouble(3.5))
        );
    }
}
