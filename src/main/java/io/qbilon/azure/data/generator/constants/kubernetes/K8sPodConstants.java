package io.qbilon.azure.data.generator.constants.kubernetes;

public class K8sPodConstants {
  public static final String RESOURCE_ID_POD = "K8s Pod";
  public static final String NODE_NAME = "Node Name";
  public static final String HOSTNAME = "Hostname";
  public static final String POD_IP = "Pod IP";
  public static final String HOST_IP = "Host IP";
}
