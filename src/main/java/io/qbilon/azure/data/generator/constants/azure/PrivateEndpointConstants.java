package io.qbilon.azure.data.generator.constants.azure;

public class PrivateEndpointConstants {
  public static final String RESOURCE_NAME_PRIVATE_ENDPOINT = "Azure Private Endpoint";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String NAME = "Name";
  public static final String NETWORK_TYPE = "Network Type";
}
