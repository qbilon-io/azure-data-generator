package io.qbilon.azure.data.generator.constants.azure;

public class ApplicationGatewayConstants {
  public static final String RESOURCE_NAME_APPLICATION_GATEWAY = "Azure Virtual Gateway";

  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String PRIVATE_IP_ADDRESS = "Private IP Address";
  public static final String LISTENING_FOR_HTTP_PORT = "Listening for HTTP Port";
  public static final String LISTENING_FOR_HTTPS_PORT = "Listening for HTTPS Port";
  public static final String AUTOSCALING_MAX_CAPACITY = "Autoscaling Max. Capacity";
  public static final String AUTOSCALING_MIN_CAPACITY = "Autoscaling Min. Capacity";
  public static final String NUMBER_INSTANCES = "Number Instances";
  public static final String HTTP2_ENABLED = "HTTP2 Enabled";
  public static final String IS_PRIVATE_GATEWAY = "Is private gateway";
  public static final String IS_PUBLIC_GATEWAY = "Is public gateway";
  public static final String GATEWAY_SKU = "Gateway SKU";
  public static final String GATEWAY_SKU_CAPACITY = "Gateway SKU Capacity";
  public static final String GATEWAY_TYPE = "Gateway Type";
}
