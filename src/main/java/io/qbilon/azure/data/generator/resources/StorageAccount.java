package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.constants.azure.StorageAccountConstants.*;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class StorageAccount implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_STORAGE_ACCOUNT;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_STORAGE_ACCOUNT,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return new ArrayList<>(Arrays.asList(
            property(AVERAGE_EGRESS_MB_7_D, AzureDataHelper.randomInt(100)),
            property(AVERAGE_INGRESS_MB_7_D, AzureDataHelper.randomInt(100)),
            property(AZURE_FILES_AAD_INTEGRATION_ENABLED, AzureDataHelper.randomBool()),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_CREATED),
            azureTag(TAG_INFORMATION_CLASSIFICATION),
            azureTag(TAG_K8S_AZURE_CREATED_BY),
            azureTag(TAG_MS_RESOURCE_USAGE),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_SERVICE),
            azureTag(TAG_TEAM),
            azureTag(TAG_TEAMS),
            azureTag(TAG_USER),
            property(BLOB_PUBLIC_ACCESS_ALLOWED, AzureDataHelper.randomBool()),
            property(CAN_ACCESS_FROM_AZURE_SERVICES, AzureDataHelper.randomBool()),
            property(CAN_READ_LOG_ENTRIES_FROM_ANY_NETWORK, AzureDataHelper.randomBool()),
            property(CAN_READ_METRICS_FROM_ANY_NETWORK, AzureDataHelper.randomBool()),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(HNS_ENABLED, AzureDataHelper.randomBool()),
            property(HTTPS_TRAFFIC_ONLY, AzureDataHelper.randomBool()),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(IS_ACCESS_ALLOWED_FROM_ALL_NETWORKS, AzureDataHelper.randomBool()),
            property(KIND, "StorageV2"),
            property(LARGE_FILE_SHARES_ENABLED, AzureDataHelper.randomBool()),
            property(MINIMUM_TLS_VERSION, "TLS1_2"),
            property(NAME, AzureDataHelper.randomName("Storage-Account")),
            property(NUMBER_TRANSACTIONS_7_D, AzureDataHelper.randomInt(1000)),
            property(PROVISIONING_STATE, "Succeeded"),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE),
            property(SHARED_KEY_ACCESS_ALLOWED, AzureDataHelper.randomBool()),
            property(SKU_TYPE, "Standard_LRS"),
            property(USED_CAPACITY_MB, AzureDataHelper.randomInt(10_000))
        ));
    }
}
