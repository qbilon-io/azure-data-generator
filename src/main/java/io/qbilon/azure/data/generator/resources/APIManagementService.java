package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.ApiManagementConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class APIManagementService implements AzureNodeGenerator {

    @Override
    public String type() {
        return RESOURCE_NAME_API_MANAGEMENT_SERVICE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_API_MANAGEMENT_SERVICE,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            azureTag(TAG_UPDATED_BY_SCRIPT),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(GATEWAY_URL, "azurewebsites.net"),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(MANAGEMENT_API_URL, "azurewebsites.net"),
            property(NAME, AzureDataHelper.randomName("API-Management-Service")),
            property(SKU, SKU),
            property(SKU_CAPACITY, AzureDataHelper.randomInt(100_000))
        );
    }
}
