package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.NetworkInterfaceConstants.PRIVATE_IP_ADDRESS;
import static io.qbilon.azure.data.generator.constants.azure.NetworkInterfaceConstants.RESOURCE_NAME_NETWORK_INTERFACE;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class NetworkInterface implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_NETWORK_INTERFACE;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_NETWORK_INTERFACE,
            PRIVATE_IP_ADDRESS,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_PROJECT),
            azureTag(TAG_SOLUTION),
            azureTag(TAG_STAGE),
            azureTag(TAG_TEAM),
            azureTag(TAG_USER),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(PRIVATE_IP_ADDRESS, AzureDataHelper.randomIP()),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE)
        );
    }
}
