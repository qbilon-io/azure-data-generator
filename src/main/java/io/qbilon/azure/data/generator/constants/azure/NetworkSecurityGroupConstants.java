package io.qbilon.azure.data.generator.constants.azure;

public class NetworkSecurityGroupConstants {
  public static final String RESOURCE_NAME_NETWORK_SECURITY_GROUP = "Azure Network Security Group";
  public static final String RELATION_NAME_CONTAINS = "contains";
  public static final String RELATION_NAME_SECURED_BY = "secured by";
  public static final String NAME = "Name";
  public static final String ALLOWS_ALL_THROUGH_TRAFFIC = "Allows all through traffic";
  public static final String OPEN_PORT_FOR_SSH = "Open Port for SSH";
  public static final String OPEN_PORT_FOR_HTTP = "Open Port for HTTP";
  public static final String OPEN_PORT_FOR_HTTPS = "Open Port for HTTPS";
  public static final String OPEN_PORT_FOR_RDP = "Open Port for RDP";
  public static final String OPEN_PORT_FOR_RPC = "Open Port for RPC";
  public static final String OPEN_PORT_FOR_MS_SQL = "Open Port for MsSQL";
  public static final String OPEN_PORT_FOR_FTP = "Open Port for FTP";
  public static final String OPEN_PORT_FOR_TELNET = "Open Port for Telnet";
  public static final String OPEN_PORT_FOR_SMTP = "Open Port for SMTP";
  public static final String OPEN_PORT_FOR_MONGO_DB = "Open Port for MongoDB";
  public static final String OPEN_PORT_FOR_MY_SQL = "Open Port for MySQL";
  public static final String OPEN_PORT_FOR_CIFS = "Open Port for CIFS";
  public static final String OPEN_PORT_FOR_DNS = "Open Port for DNS";
  public static final String OPEN_PORT_FOR_POSTGESQL = "Open Port for PostgeSQL";
  public static final String OPEN_PORT_FOR_ELASTICSEARCH = "Open Port for Elasticsearch";
  public static final String OPEN_PORT_FOR_ORACLE = "Open Port for Oracle";
}
