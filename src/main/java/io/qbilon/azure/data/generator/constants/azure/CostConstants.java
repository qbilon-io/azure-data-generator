package io.qbilon.azure.data.generator.constants.azure;

public class CostConstants {
  public static final String MONTH_TO_DATE_COSTS_PROP = "Current Month Costs";
  public static final String COSTS_LAST_MONTH_PROP = "Costs Last Month";
}
