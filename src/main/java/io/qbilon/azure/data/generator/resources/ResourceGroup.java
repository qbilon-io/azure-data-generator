package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.GeneralAzureConstants.AZURE_ID;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.PROVISIONING_STATE;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_NAME_RESOURCE_GROUP;
import static io.qbilon.azure.data.generator.constants.azure.VirtualMachineConstants.NAME;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class ResourceGroup implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_RESOURCE_GROUP;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_RESOURCE_GROUP,
            NAME,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_AKS_MANAGED_CLUSTER_NAME),
            azureTag(TAG_AKS_MANAGED_CLUSTER_RG),
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_APP_SENSOR),
            azureTag(TAG_CREATED),
            azureTag(TAG_CREATED_BY),
            azureTag(TAG_CREATION_TIME),
            azureTag(TAG_DISPLAY_NAME),
            azureTag(TAG_ENVIRONMENT),
            azureTag(TAG_INFORMATION_CLASSIFICATION),
            azureTag(TAG_OWNER),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_PROJECT),
            azureTag(TAG_SERVICE),
            azureTag(TAG_SET_CONTRIBUTOR),
            azureTag(TAG_TEAM),
            azureTag(TAG_USER),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(AZURE_ID, AzureDataHelper.randomId()),
            property(NAME, AzureDataHelper.randomName("Resource-Group")),
            property(PROVISIONING_STATE, "Succeeded")
        );
    }
}
