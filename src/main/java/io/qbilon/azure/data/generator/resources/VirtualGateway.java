package io.qbilon.azure.data.generator.resources;

import static io.qbilon.azure.data.generator.Constants.*;
import static io.qbilon.azure.data.generator.constants.azure.ApplicationGatewayConstants.*;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.COSTS_LAST_MONTH_PROP;
import static io.qbilon.azure.data.generator.constants.azure.CostConstants.MONTH_TO_DATE_COSTS_PROP;
import static io.qbilon.azure.data.generator.constants.azure.ResourceGroupConstants.RESOURCE_GROUP_ATTRIBUTE;
import static io.qbilon.azure.data.generator.data.input.Node.node;
import static io.qbilon.azure.data.generator.data.input.Property.property;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.qbilon.azure.data.generator.AzureDataHelper;
import io.qbilon.azure.data.generator.data.input.Node;
import io.qbilon.azure.data.generator.data.input.Property;

public class VirtualGateway implements AzureNodeGenerator {
    @Override
    public String type() {
        return RESOURCE_NAME_APPLICATION_GATEWAY;
    }

    @Override
    public Node generate() {
        return node(
            UUID.randomUUID().toString(),
            RESOURCE_NAME_APPLICATION_GATEWAY,
            GATEWAY_TYPE,
            properties()
        );
    }

    private List<Property> properties() {
        return Arrays.asList(
            azureTag(TAG_COST_CENTER),
            azureTag(TAG_INGRESS_FOR_AKS_CLUSTER_ID),
            azureTag(TAG_MANAGED_BY_K8S_INGRESS),
            azureTag(TAG_PRODUCT),
            azureTag(TAG_TEAM),
            property(COSTS_LAST_MONTH_PROP, AzureDataHelper.randomInt(10_000)),
            property(MONTH_TO_DATE_COSTS_PROP, AzureDataHelper.randomInt(10_000)),
            property(GATEWAY_SKU, GATEWAY_SKU),
            property(GATEWAY_SKU_CAPACITY, AzureDataHelper.randomInt(100_000)),
            property(GATEWAY_TYPE, GATEWAY_TYPE),
            property(HTTP2_ENABLED, AzureDataHelper.randomBool()),
            property(IS_PRIVATE_GATEWAY, AzureDataHelper.randomBool()),
            property(IS_PUBLIC_GATEWAY, AzureDataHelper.randomBool()),
            property(LISTENING_FOR_HTTP_PORT, AzureDataHelper.randomBool()),
            property(LISTENING_FOR_HTTPS_PORT, AzureDataHelper.randomBool()),
            property(NUMBER_INSTANCES, AzureDataHelper.randomInt(100_000)),
            property(RESOURCE_GROUP_ATTRIBUTE, RESOURCE_GROUP_ATTRIBUTE)
        );
    }
}
