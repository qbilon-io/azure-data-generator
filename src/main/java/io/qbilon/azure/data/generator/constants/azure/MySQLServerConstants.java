package io.qbilon.azure.data.generator.constants.azure;

public class MySQLServerConstants {

  public static final String RESOURCE_NAME_MYSQL_SERVER = "Azure MySQL Server";
}
