package io.qbilon.azure.data.generator.data.datatype;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;

public class DatatypeSerializer extends StdSerializer<Datatype> {

  public DatatypeSerializer() {
    this(null);
  }

  protected DatatypeSerializer(Class<Datatype> t) {
    super(t);
  }

  @Override
  public void serialize(Datatype value, JsonGenerator gen, SerializerProvider provider)
      throws IOException {
    gen.writeString(value.toString());
  }
}
